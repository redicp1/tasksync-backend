import http from 'k6/http';
import { sleep, check } from 'k6';

export let options = {
  ext: {
    loadimpact: {
      projectID: 3701974,
    },
  },
  stages: [
    { duration: '30s', target: 20 },
    { duration: '1m', target: 20 },
    { duration: '10s', target: 0 },
  ],
};

const credentials = JSON.stringify({
  email: 'red@gmail.com',
  password: '123Red',
});

export default function () {
  let loginRes = http.post(
    'http://localhost:3000/api/users/login',
    credentials,
    {
      headers: { 'Content-Type': 'application/json' },
    },
  );

  const token = loginRes.body;

  check(loginRes, { 'login successful': (r) => r.status === 200 });

  let authToken = JSON.parse(token);
  let urlToken = JSON.parse(token);

  // Define the request headers
  let header = {
    headers: {
      Authorization: `Bearer ${authToken}`,
      'Content-Type': 'application/json',
    },
  };

  let getOwnedProjects = http.get(
    `http://localhost:3000/api/users/${urlToken}/ownedprojects`,
    header,
  );

  check(getOwnedProjects, { 'get owned projects': (r) => r.status === 200 });

  let getUserProjects = http.get(
    `http://localhost:3000/api/users/${urlToken}/projects`,
    header,
  );

  check(getUserProjects, { 'get user projects': (r) => r.status === 200 });

  let getProjectMembers = http.get(
    'http://localhost:3000/api/projects/2/members',
    header,
  );

  check(getProjectMembers, { 'get project members': (r) => r.status === 200 });

  let getProjectSprints = http.get(
    'http://localhost:3000/api/projects/2/sprints',
    header,
  );

  check(getProjectSprints, { 'get project sprints': (r) => r.status === 200 });

  let getProjectTasks = http.get(
    'http://localhost:3000/api/projects/2/tasks',
    header,
  );

  check(getProjectTasks, { 'get project tasks': (r) => r.status === 200 });

  let getSprintTasks = http.get(
    'http://localhost:3000/api/2/sprints/2/tasks',
    header,
  );

  check(getSprintTasks, { 'get sprint tasks': (r) => r.status === 200 });

  sleep(1);
}
