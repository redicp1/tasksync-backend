import { describe, it, expect, vi, beforeEach } from 'vitest';
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { Project } from '../../models/Project';
import { UserProject } from '../../models/UserProject';
import { authorizeMember } from '../projectMemberAuth';

vi.mock('jsonwebtoken');
vi.mock('../../models/Project');
vi.mock('../../models/UserProject');

describe('authorizeMember middleware', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;
  let next: NextFunction;

  beforeEach(() => {
    req = {
      headers: {},
      params: {},
    };
    res = {
      status: vi.fn().mockReturnThis(),
      json: vi.fn(),
    };
    next = vi.fn() as unknown as NextFunction;
  });

  it('should return 401 if no token is provided', async () => {
    req.headers!.authorization = '';

    await authorizeMember(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({ error: 'Unauthorized' });
  });

  it('should return 404 if project is not found', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.projectId = '1';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (Project.findByPk as any).mockResolvedValueOnce(null);

    await authorizeMember(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({ error: 'Project not found' });
  });

  it('should return 403 if user is not a member and not the owner', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.projectId = '1';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (Project.findByPk as any).mockResolvedValueOnce({
      id: '1',
      ownerId: '2',
    });
    (UserProject.findOne as any).mockResolvedValueOnce(null);

    await authorizeMember(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(403);
    expect(res.json).toHaveBeenCalledWith({
      error: 'Forbidden: You are not the member of this project',
    });
  });

  it('should call next if user is a member or the owner', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.projectId = '1';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (Project.findByPk as any).mockResolvedValueOnce({
      id: '1',
      ownerId: '1',
    });

    await authorizeMember(req as Request, res as Response, next);

    expect(next).toHaveBeenCalled();
  });

  it('should call next if user is a member', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.projectId = '1';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (Project.findByPk as any).mockResolvedValueOnce({
      id: '1',
      ownerId: '2',
    });
    (UserProject.findOne as any).mockResolvedValueOnce({
      userId: '1',
      projectId: '1',
    });

    await authorizeMember(req as Request, res as Response, next);

    expect(next).toHaveBeenCalled();
  });

  it('should return 400 if an error occurs', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.projectId = '1';

    (jwt.verify as any).mockImplementation(() => {
      throw new Error('Token error');
    });

    await authorizeMember(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      error: 'Invalid token or project ID',
    });
  });
});
