import { describe, it, expect, vi, beforeEach } from 'vitest';
import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { User } from '../../models/User';
import { authorizeUser } from '../accountAuth';

vi.mock('jsonwebtoken');

vi.mock('../../models/User', () => ({
  User: {
    findByPk: vi.fn(),
  },
}));

describe('authorizeUser middleware', () => {
  let req: Partial<Request>;
  let res: Partial<Response>;
  let next: NextFunction;

  beforeEach(() => {
    req = {
      headers: {},
      params: {},
    };
    res = {
      status: vi.fn().mockReturnThis(),
      json: vi.fn(),
    };
    next = vi.fn() as any;
  });

  it('should return 401 if no token is provided', async () => {
    req.headers!.authorization = '';

    await authorizeUser(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({ error: 'Unauthorized' });
  });

  it('should return 401 if token and param token do not match', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.token = 'paramtoken';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (jwt.verify as any).mockReturnValueOnce({ userId: '2' });

    await authorizeUser(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({ error: 'Unauthorized' });
  });

  it('should return 403 if user does not exist', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.token = 'paramtoken';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });

    (User.findByPk as any).mockResolvedValue(null);

    await authorizeUser(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(403);
    expect(res.json).toHaveBeenCalledWith({
      error: 'Forbidden: You are not a user of TaskSync',
    });
  });

  it('should call next if user is authorized', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.token = 'paramtoken';

    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });
    (jwt.verify as any).mockReturnValueOnce({ userId: '1' });

    (User.findByPk as any).mockResolvedValueOnce({ id: '1' });

    await authorizeUser(req as Request, res as Response, next);

    expect(next).toHaveBeenCalled();
  });

  it('should return 400 if an error occurs', async () => {
    req.headers!.authorization = 'Bearer validtoken';
    req.params!.token = 'paramtoken';

    (jwt.verify as any).mockImplementation(() => {
      throw new Error('Token error');
    });

    await authorizeUser(req as Request, res as Response, next);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ error: 'Authorization Error' });
  });
});
