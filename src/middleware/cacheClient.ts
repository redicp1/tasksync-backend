import Redis from 'ioredis';
import * as dotenv from 'dotenv';

dotenv.config();

const redisClient = new Redis({
  host: 'redis',
  port: Number(process.env.REDIS_LOCAL_PORT),
});

export default redisClient;
