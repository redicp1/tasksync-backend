import { Request, Response, NextFunction } from 'express';
import { Project } from '../models/Project';
import jwt from 'jsonwebtoken';
import { UserProject } from '../models/UserProject';

export async function authorizeMember(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(401).json({ error: 'Unauthorized' });
  }

  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const userId = decoded.userId;
    const projectId = req.params.projectId;

    const project = await Project.findByPk(projectId);

    if (!project) {
      return res.status(404).json({ error: 'Project not found' });
    }

    const isMember = await UserProject.findOne({
      where: { userId: userId, projectId: projectId },
    });

    if (!isMember && project.ownerId !== userId) {
      return res
        .status(403)
        .json({ error: 'Forbidden: You are not the member of this project' });
    }

    next();
  } catch (error: any) {
    res.status(400).json({ error: 'Invalid token or project ID' });
  }
}
