import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import { User } from '../models/User';

export async function authorizeUser(
  req: Request,
  res: Response,
  next: NextFunction,
) {
  try {
    const token = req.headers.authorization?.split(' ')[1];
    const paramToken = req.params.token;

    if (!token) {
      return res.status(401).json({ error: 'Unauthorized' });
    }

    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const decodedParam: any = jwt.verify(
      paramToken,
      String(process.env.SECRET_KEY),
    );

    const userId = decoded.userId;
    const paramId = decodedParam.userId;

    if (userId != paramId) {
      return res.status(401).json({ error: 'Unauthorized' });
    }

    const user = await User.findByPk(userId);

    if (!user) {
      return res
        .status(403)
        .json({ error: 'Forbidden: You are not a user of TaskSync' });
    }

    next();
  } catch (error: any) {
    res.status(400).json({ error: 'Authorization Error' });
  }
}
