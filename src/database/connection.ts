import { Sequelize } from 'sequelize-typescript';
import { User } from '../models/User';
import { Project } from '../models/Project';
import { Sprint } from '../models/Sprint';
import { Task } from '../models/Task';
import { Notification } from '../models/Notification';
import { UserProject } from '../models/UserProject';
import { TaskAssignee } from '../models/TaskAssignee';
import * as dotenv from 'dotenv';

dotenv.config();

const sequelize = new Sequelize({
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  host: 'mysqldb',
  port: Number(process.env.DB_LOCAL_PORT),
  database: process.env.DB_DATABASE,
  dialect: 'mysql',
  models: [
    User,
    Project,
    Sprint,
    Task,
    Notification,
    UserProject,
    TaskAssignee,
  ],
});

export default sequelize;
