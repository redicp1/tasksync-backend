import express from 'express';
import bodyParser from 'body-parser';
import userController from './controllers/userController';
import projectController from './controllers/projectController';
import sprintController from './controllers/sprintController';
import taskContoller from './controllers/taskController';
import sequelize from './database/connection';
import * as dotenv from 'dotenv';
import cors from 'cors';
import helmet from 'helmet';

dotenv.config();

const app = express();

app.disable('x-powered-by');
app.use(cors());
app.use(helmet());

const PORT = 3000;

app.use(bodyParser.json());
app.use('/api', userController);
app.use('/api', projectController);
app.use('/api', sprintController);
app.use('/api', taskContoller);

sequelize.sync().then(() => {
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
});
