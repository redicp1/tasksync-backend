import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  DeletedAt,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { User } from './User';
import { Sprint } from './Sprint';
import { UserProject } from './UserProject';
import { Task } from './Task';

@Table({ tableName: 'Project', paranoid: true })
export class Project extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string;

  @Column(DataType.STRING)
  description: string;

  @BelongsTo(() => User)
  owner: User;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  ownerId: number;

  @BelongsToMany(() => User, () => UserProject)
  members: User[];

  @HasMany(() => Sprint)
  sprints: Sprint[];

  @HasMany(() => Task)
  tasks: Task[];

  @CreatedAt
  created: Date;

  @UpdatedAt
  updated: Date;

  @DeletedAt
  deleted: Date;
}
