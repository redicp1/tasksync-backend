import {
  Table,
  Column,
  Model,
  DataType,
  ForeignKey,
  AllowNull,
} from 'sequelize-typescript';
import { User } from './User';
import { Task } from './Task';

@Table({ tableName: 'TaskAssignee', paranoid: true })
export class TaskAssignee extends Model {
  @ForeignKey(() => User)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  userId: number;

  @ForeignKey(() => Task)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  taskId: number;
}
