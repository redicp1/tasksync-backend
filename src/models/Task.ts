import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  DeletedAt,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { User } from './User';
import { Sprint } from './Sprint';
import { Project } from './Project';

@Table({ tableName: 'Task', paranoid: true })
export class Task extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  title: string;

  @AllowNull(true)
  @Column(DataType.STRING)
  description: string;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  authorId: number;

  @BelongsTo(() => User)
  author: User;

  @AllowNull(true)
  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  assigneeId: number | null;

  @BelongsTo(() => User)
  assignee: User | null;

  @AllowNull(true)
  @ForeignKey(() => Sprint)
  @Column(DataType.INTEGER)
  sprintId: number | null;

  @BelongsTo(() => Sprint)
  sprint: Sprint | null;

  @ForeignKey(() => Project)
  @Column(DataType.INTEGER)
  projectId: number;

  @BelongsTo(() => Project)
  project: Project;

  @AllowNull(true)
  @Column(DataType.INTEGER)
  storyPoint: number;

  @CreatedAt
  created: Date;

  @UpdatedAt
  updated: Date;

  @DeletedAt
  deleted: Date;
}
