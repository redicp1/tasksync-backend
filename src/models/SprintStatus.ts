export enum SprintStatus {
  COMPLETE = 'complete',
  RUNNING = 'running',
  UNBEGUN = 'unbegun',
}
