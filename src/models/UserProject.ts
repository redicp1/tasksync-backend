import {
  Table,
  Column,
  Model,
  ForeignKey,
  AllowNull,
  DataType,
} from 'sequelize-typescript';
import { User } from './User';
import { Project } from './Project';

@Table({ tableName: 'UserProject', paranoid: true })
export class UserProject extends Model {
  @ForeignKey(() => User)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  userId: number;

  @ForeignKey(() => Project)
  @AllowNull(false)
  @Column(DataType.INTEGER)
  projectId: number;
}
