import {
  AllowNull,
  AutoIncrement,
  BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  DeletedAt,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { Project } from './Project';
import { Notification } from './Notification';
import { Task } from './Task';
import { UserProject } from './UserProject';
import { TaskAssignee } from './TaskAssignee';

@Table({ tableName: 'User', paranoid: true })
export class User extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  username: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  email: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  password: string;

  @HasMany(() => Project)
  ownedProjects: Project[];

  @BelongsToMany(() => Project, () => UserProject)
  projects: Project[];

  @HasMany(() => Notification)
  notifications: Notification[];

  @HasMany(() => Task)
  authoredTasks: Task[];

  @BelongsToMany(() => Task, () => TaskAssignee)
  assignedTasks: Task[];

  @CreatedAt
  created: Date;

  @UpdatedAt
  updated: Date;

  @DeletedAt
  deleted: Date;
}
