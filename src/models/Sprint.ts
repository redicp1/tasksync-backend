import {
  AllowNull,
  AutoIncrement,
  BelongsTo,
  Column,
  CreatedAt,
  DataType,
  Default,
  DeletedAt,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { Task } from './Task';
import { Project } from './Project';
import { SprintStatus } from './SprintStatus';

@Table({ tableName: 'Sprint', paranoid: true })
export class Sprint extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column(DataType.INTEGER)
  id: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  name: string;

  @Column(DataType.STRING)
  goal: string;

  @Column(DataType.DATE)
  startDate: Date;

  @Column(DataType.DATE)
  endDate: Date;

  @AllowNull(false)
  @Default(SprintStatus.UNBEGUN)
  @Column(
    DataType.ENUM(
      SprintStatus.UNBEGUN,
      SprintStatus.COMPLETE,
      SprintStatus.RUNNING,
    ),
  )
  status: SprintStatus;

  @BelongsTo(() => Project)
  project: Project;

  @ForeignKey(() => Project)
  @Column(DataType.INTEGER)
  projectId: number;

  @HasMany(() => Task)
  tasks: Task[];

  @CreatedAt
  created: Date;

  @UpdatedAt
  updated: Date;

  @DeletedAt
  deleted: Date;
}
