import express, { Request, Response } from 'express';
import * as createService from '../services/project/create';
import * as updateService from '../services/project/update';
import * as deleteService from '../services/project/delete';
import * as projectSprintsService from '../services/project/projectSprints';
import * as projectTasksService from '../services/project/backlogTasks';
import * as projectMemberService from '../services/project/projectMembers';
import * as addProjectMember from '../services/project/addProjectMember';
import { authorizeMember } from '../middleware/projectMemberAuth';
import { authorizeUser } from '../middleware/accountAuth';

const router = express.Router();

//Get members of a project------------------------------------------------------------
router.get(
  '/projects/:projectId/members',
  authorizeMember,
  async (req: Request, res: Response) => {
    const projectId = req.params.projectId;

    try {
      const projects = await projectMemberService.getProjectMembers(
        Number(projectId),
      );

      res.json(projects);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Get sprints of a project-----------------------------------------------------------
router.get(
  '/projects/:projectId/sprints',
  authorizeMember,
  async (req: Request, res: Response) => {
    const projectId = req.params.projectId;

    try {
      const projects = await projectSprintsService.getProjectSprints(
        Number(projectId),
      );

      res.json(projects);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Get tasks of a project-------------------------------------------------------------
router.get(
  '/projects/:projectId/tasks',
  authorizeMember,
  async (req: Request, res: Response) => {
    const projectId = req.params.projectId;

    try {
      const projects = await projectTasksService.getBacklogTasks(
        Number(projectId),
      );

      res.json(projects);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Creating project--------------------------------------------------------------------
router.post(
  '/:token/projects',
  authorizeUser,
  async (req: Request, res: Response) => {
    const { name, description } = req.body;
    const token = req.params.token;

    try {
      const project = await createService.createProject(
        name,
        description,
        token,
      );

      res.json(project);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Adding member to a project------------------------------------------------------------
router.post(
  '/projects/:projectId/members',
  authorizeMember,
  async (req: Request, res: Response) => {
    const projectId = req.params.projectId;
    const { userId } = req.body;

    try {
      await addProjectMember.addProjectMember(userId, Number(projectId));

      res.status(200).json('OK');
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Updating a project details-------------------------------------------------------------
router.put(
  '/projects/:projectId',
  authorizeMember,
  async (req: Request, res: Response) => {
    const projectId = req.params.projectId;
    const { name, description } = req.body;

    try {
      const project = await updateService.updateProject(
        Number(projectId),
        name,
        description,
      );

      res.json(project);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Deleting a project----------------------------------------------------------------------
router.delete(
  '/projects/:projectId',
  authorizeMember,
  async (req: Request, res: Response) => {
    const projectId = req.params.projectId;

    try {
      await deleteService.deleteProject(Number(projectId));

      res.status(200).json('OK');
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  },
);

export default router;
