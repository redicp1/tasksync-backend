import request from 'supertest';
import express, { Request, Response, NextFunction } from 'express';
import router from '../sprintController';
import * as createService from '../../services/sprint/create';
import * as updateService from '../../services/sprint/update';
import * as updateStatusService from '../../services/sprint/updateStatus';
import * as deleteService from '../../services/sprint/delete';
import * as sprintTasksService from '../../services/sprint/sprintTasks';
import { authorizeMember } from '../../middleware/projectMemberAuth';
import { vi, describe, it, expect, beforeEach } from 'vitest';

vi.mock('../../services/sprint/create');
vi.mock('../../services/sprint/update');
vi.mock('../../services/sprint/updateStatus');
vi.mock('../../services/sprint/delete');
vi.mock('../../services/sprint/sprintTasks');
vi.mock('../../middleware/projectMemberAuth');

const app = express();
app.use(express.json());
app.use(router);

describe('Sprint Router', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  const startDate = new Date('2023-06-01');
  const endDate = new Date('2023-06-15');

  it('should get tasks of a sprint', async () => {
    const sprintId = 1;
    const tasks = [
      { id: 1, title: 'Task 1' },
      { id: 2, title: 'Task 2' },
    ];
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (sprintTasksService.getSprintTasks as any).mockResolvedValue(tasks);

    const res = await request(app).get(`/2/sprints/${sprintId}/tasks`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(tasks);
    expect(sprintTasksService.getSprintTasks).toHaveBeenCalledWith(sprintId);
  });

  it('should create a sprint', async () => {
    const sprint = {
      id: 1,
      name: 'Sprint 1',
      goal: 'Goal 1',
      startDate: String(startDate),
      endDate: String(endDate),
      projectId: 1,
    };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (createService.createSprint as any).mockResolvedValue(sprint);

    const res = await request(app)
      .post('/2/sprints')
      .send({
        name: 'Sprint 1',
        goal: 'Goal 1',
        startDate: String(startDate),
        endDate: String(endDate),
        projectId: 1,
      });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(sprint);
    expect(createService.createSprint).toHaveBeenCalledWith(
      'Sprint 1',
      'Goal 1',
      String(startDate),
      String(endDate),
      1,
    );
  });

  it('should update a sprint', async () => {
    const sprintId = 1;
    const sprint = {
      id: 1,
      name: 'Updated Sprint',
      goal: 'Updated Goal',
      startDate: String(startDate),
      endDate: String(endDate),
    };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateService.updateSprint as any).mockResolvedValue(sprint);

    const res = await request(app)
      .put(`/2/sprints/${sprintId}`)
      .send({
        name: 'Updated Sprint',
        goal: 'Updated Goal',
        startDate: String(startDate),
        endDate: String(endDate),
      });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(sprint);
    expect(updateService.updateSprint).toHaveBeenCalledWith(
      sprintId,
      'Updated Sprint',
      'Updated Goal',
      String(startDate),
      String(endDate),
    );
  });

  it('should start a sprint', async () => {
    const sprintId = 1;
    const sprint = { id: 1, name: 'Sprint 1', status: 'started' };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateStatusService.startSprint as any).mockResolvedValue(sprint);

    const res = await request(app).put(`/2/sprints/${sprintId}/status/start`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(sprint);
    expect(updateStatusService.startSprint).toHaveBeenCalledWith(sprintId);
  });

  it('should end a sprint', async () => {
    const sprintId = 1;
    const nextSprintId = 2;
    const sprint = { id: 1, name: 'Sprint 1', status: 'ended' };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateStatusService.endSprint as any).mockResolvedValue(sprint);

    const res = await request(app)
      .put(`/2/sprints/${sprintId}/status/end`)
      .send({ nextSprintId });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(sprint);
    expect(updateStatusService.endSprint).toHaveBeenCalledWith(
      sprintId,
      nextSprintId,
    );
  });

  it('should delete a sprint', async () => {
    const sprintId = 1;
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (deleteService.deleteSprint as any).mockResolvedValue();

    const res = await request(app).delete(`/2/sprints/${sprintId}`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual('OK');
    expect(deleteService.deleteSprint).toHaveBeenCalledWith(sprintId);
  });

  it('should return an error if sprint task retrieval fails', async () => {
    const sprintId = 1;
    const errorMessage = 'Database error';
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (sprintTasksService.getSprintTasks as any).mockRejectedValue(
      new Error(errorMessage),
    );

    const res = await request(app).get(`/2/sprints/${sprintId}/tasks`);

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });

  // Repeat similar error test cases for other endpoints if needed
});
