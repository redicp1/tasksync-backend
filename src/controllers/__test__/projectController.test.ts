import request from 'supertest';
import express, { Request, Response, NextFunction } from 'express';
import router from '../projectController';
import * as createService from '../../services/project/create';
import * as updateService from '../../services/project/update';
import * as deleteService from '../../services/project/delete';
import * as projectSprintsService from '../../services/project/projectSprints';
import * as projectTasksService from '../../services/project/backlogTasks';
import * as projectMemberService from '../../services/project/projectMembers';
import * as addProjectMember from '../../services/project/addProjectMember';
import { authorizeMember } from '../../middleware/projectMemberAuth';
import { authorizeUser } from '../../middleware/accountAuth';
import { vi, describe, it, expect, beforeEach } from 'vitest';

vi.mock('../../services/project/create');
vi.mock('../../services/project/update');
vi.mock('../../services/project/delete');
vi.mock('../../services/project/projectSprints');
vi.mock('../../services/project/backlogTasks');
vi.mock('../../services/project/projectMembers');
vi.mock('../../services/project/addProjectMember');
vi.mock('../../middleware/projectOwnerAuth');
vi.mock('../../middleware/projectMemberAuth');
vi.mock('../../middleware/accountAuth');

const app = express();
app.use(express.json());
app.use(router);

describe('Project Router', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should get project members', async () => {
    const projectId = 1;
    const members = [
      { id: 1, name: 'User 1' },
      { id: 2, name: 'User 2' },
    ];
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (projectMemberService.getProjectMembers as any).mockResolvedValue(members);

    const res = await request(app).get(`/projects/${projectId}/members`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(members);
    expect(projectMemberService.getProjectMembers).toHaveBeenCalledWith(
      projectId,
    );
  });

  it('should get project sprints', async () => {
    const projectId = 1;
    const sprints = [
      { id: 1, name: 'Sprint 1' },
      { id: 2, name: 'Sprint 2' },
    ];
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (projectSprintsService.getProjectSprints as any).mockResolvedValue(sprints);

    const res = await request(app).get(`/projects/${projectId}/sprints`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(sprints);
    expect(projectSprintsService.getProjectSprints).toHaveBeenCalledWith(
      projectId,
    );
  });

  it('should get project tasks', async () => {
    const projectId = 1;
    const tasks = [
      { id: 1, name: 'Task 1' },
      { id: 2, name: 'Task 2' },
    ];
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (projectTasksService.getBacklogTasks as any).mockResolvedValue(tasks);

    const res = await request(app).get(`/projects/${projectId}/tasks`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(tasks);
    expect(projectTasksService.getBacklogTasks).toHaveBeenCalledWith(projectId);
  });

  it('should create a project', async () => {
    const project = {
      id: 1,
      name: 'Project 1',
      description: 'Description 1',
    };
    (authorizeUser as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (createService.createProject as any).mockResolvedValue(project);

    const res = await request(app).post('/2/projects').send({
      name: 'Project 1',
      description: 'Description 1',
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(project);
    expect(createService.createProject).toHaveBeenCalledWith(
      'Project 1',
      'Description 1',
      '2',
    );
  });

  it('should add a member to a project', async () => {
    const projectId = 1;
    const userId = 2;
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (addProjectMember.addProjectMember as any).mockResolvedValue();

    const res = await request(app)
      .post(`/projects/${projectId}/members`)
      .send({ userId });

    expect(res.status).toBe(200);
    expect(res.body).toEqual('OK');
    expect(addProjectMember.addProjectMember).toHaveBeenCalledWith(
      userId,
      projectId,
    );
  });

  it('should update a project', async () => {
    const projectId = 1;
    const project = {
      id: 1,
      name: 'Updated Project',
      description: 'Updated Description',
    };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateService.updateProject as any).mockResolvedValue(project);

    const res = await request(app).put(`/projects/${projectId}`).send({
      name: 'Updated Project',
      description: 'Updated Description',
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(project);
    expect(updateService.updateProject).toHaveBeenCalledWith(
      projectId,
      'Updated Project',
      'Updated Description',
    );
  });

  it('should delete a project', async () => {
    const projectId = 1;
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (deleteService.deleteProject as any).mockResolvedValue();

    const res = await request(app).delete(`/projects/${projectId}`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual('OK');
    expect(deleteService.deleteProject).toHaveBeenCalledWith(projectId);
  });

  it('should return an error if project member retrieval fails', async () => {
    const projectId = 1;
    const errorMessage = 'Database error';
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (projectMemberService.getProjectMembers as any).mockRejectedValue(
      new Error(errorMessage),
    );

    const res = await request(app).get(`/projects/${projectId}/members`);

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });

  // Repeat similar error test cases for other endpoints if needed
});
