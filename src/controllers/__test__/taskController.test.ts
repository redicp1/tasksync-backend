import request from 'supertest';
import express, { Request, Response, NextFunction } from 'express';
import router from '../taskController';
import * as createService from '../../services/task/create';
import * as updateService from '../../services/task/update';
import * as updateAssignee from '../../services/task/updateAssignee';
import * as updateSprint from '../../services/task/updateSprint';
import * as deleteService from '../../services/task/delete';
import { authorizeMember } from '../../middleware/projectMemberAuth';
import { vi, describe, it, expect, beforeEach } from 'vitest';

vi.mock('../../services/task/create');
vi.mock('../../services/task/update');
vi.mock('../../services/task/updateAssignee');
vi.mock('../../services/task/updateSprint');
vi.mock('../../services/task/delete');
vi.mock('../../middleware/projectMemberAuth');

const app = express();
app.use(express.json());
app.use(router);

describe('Task Router', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should create a task', async () => {
    const task = {
      id: 1,
      title: 'Task 1',
      token: '1',
      projectId: 1,
      sprintId: 1,
    };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (createService.createTask as any).mockResolvedValue(task);

    const res = await request(app).post('/2/tasks').send({
      title: 'Task 1',
      token: '1',
      projectId: 1,
      sprintId: 1,
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(task);
    expect(createService.createTask).toHaveBeenCalledWith(
      'Task 1',
      undefined,
      1,
      1,
    );
  });

  it('should update a task title', async () => {
    const taskId = 1;
    const task = { id: 1, title: 'Updated Task' };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateService.updateTask as any).mockResolvedValue(task);

    const res = await request(app).put(`/2/tasks/${taskId}`).send({
      title: 'Updated Task',
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(task);
    expect(updateService.updateTask).toHaveBeenCalledWith(
      taskId,
      'Updated Task',
    );
  });

  it('should change the sprint for a task', async () => {
    const taskId = 1;
    const task = { id: 1, sprintId: 2 };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateSprint.updateTaskSprint as any).mockResolvedValue(task);

    const res = await request(app).put(`/2/tasks/${taskId}/sprint`).send({
      projectId: 1,
      sprintId: 2,
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(task);
    expect(updateSprint.updateTaskSprint).toHaveBeenCalledWith(taskId, 1, 2);
  });

  it('should change the assignee for a task', async () => {
    const taskId = 1;
    const task = { id: 1, assigneeId: 2 };
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (updateAssignee.updateTaskAssignee as any).mockResolvedValue(task);

    const res = await request(app).put(`/2/tasks/${taskId}/assignee`).send({
      projectId: 1,
      assigneeId: 2,
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(task);
    expect(updateAssignee.updateTaskAssignee).toHaveBeenCalledWith(
      taskId,
      1,
      2,
    );
  });

  it('should delete a task', async () => {
    const taskId = 1;
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (deleteService.deleteTask as any).mockResolvedValue();

    const res = await request(app).delete(`/2/tasks/${taskId}`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual('OK');
    expect(deleteService.deleteTask).toHaveBeenCalledWith(taskId);
  });

  it('should return an error if task creation fails', async () => {
    const errorMessage = 'Task creation failed';
    (authorizeMember as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (createService.createTask as any).mockRejectedValue(
      new Error(errorMessage),
    );

    const res = await request(app).post('/2/tasks').send({
      title: 'Task 1',
      authorId: 1,
      projectId: 1,
      sprintId: 1,
    });

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });

  // Repeat similar error test cases for other endpoints if needed
});
