import request from 'supertest';
import express, { Request, Response, NextFunction } from 'express';
import router from '../userController';
import * as registerService from '../../services/user/register';
import * as loginService from '../../services/user/login';
import * as userProjectsService from '../../services/user/userProjects';
import * as ownedProjectsService from '../../services/user/ownedProjects';
import { authorizeUser } from '../../middleware/accountAuth';
import { vi, describe, it, expect, beforeEach } from 'vitest';

vi.mock('../../services/user/register');
vi.mock('../../services/user/login');
vi.mock('../../services/user/userProjects');
vi.mock('../../services/user/ownedProjects');
vi.mock('../../middleware/accountAuth');

const app = express();
app.use(express.json());
app.use(router);

describe('User Router', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should get list of user projects', async () => {
    const userId = '1';
    const projects = [{ id: 1, name: 'Project 1' }];
    (authorizeUser as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (userProjectsService.getUserProjects as any).mockResolvedValue(projects);

    const res = await request(app).get(`/users/${userId}/projects`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(projects);
    expect(userProjectsService.getUserProjects).toHaveBeenCalledWith(userId);
  });

  it('should get list of owned projects', async () => {
    const userId = '1';
    const projects = [{ id: 1, name: 'Owned Project 1' }];
    (authorizeUser as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (ownedProjectsService.getOwnedProjects as any).mockResolvedValue(projects);

    const res = await request(app).get(`/users/${userId}/ownedprojects`);

    expect(res.status).toBe(200);
    expect(res.body).toEqual(projects);
    expect(ownedProjectsService.getOwnedProjects).toHaveBeenCalledWith(userId);
  });

  it('should register a user', async () => {
    const user = { id: 1, username: 'user1', email: 'user1@example.com' };
    (registerService.registerUser as any).mockResolvedValue(user);

    const res = await request(app).post('/users/register').send({
      username: 'user1',
      email: 'user1@example.com',
      password: 'password',
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(user);
    expect(registerService.registerUser).toHaveBeenCalledWith(
      'user1',
      'user1@example.com',
      'password',
    );
  });

  it('should login a user', async () => {
    const token = 'fake-jwt-token';
    (loginService.login as any).mockResolvedValue(token);

    const res = await request(app).post('/users/login').send({
      email: 'user1@example.com',
      password: 'password',
    });

    expect(res.status).toBe(200);
    expect(res.body).toEqual(token);
    expect(loginService.login).toHaveBeenCalledWith(
      'user1@example.com',
      'password',
    );
  });

  it('should return an error if getting user projects fails', async () => {
    const errorMessage = 'Failed to get task list';
    (authorizeUser as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (userProjectsService.getUserProjects as any).mockRejectedValue(
      new Error(errorMessage),
    );

    const res = await request(app).get('/users/1/projects');

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });

  it('should return an error if getting owned projects fails', async () => {
    const errorMessage = 'Failed to get task list';
    (authorizeUser as any).mockImplementation(
      (req: Request, res: Response, next: NextFunction) => next(),
    );
    (ownedProjectsService.getOwnedProjects as any).mockRejectedValue(
      new Error(errorMessage),
    );

    const res = await request(app).get('/users/1/ownedprojects');

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });

  it('should return an error if user registration fails', async () => {
    const errorMessage = 'Email is already registered';
    (registerService.registerUser as any).mockRejectedValue(
      new Error(errorMessage),
    );

    const res = await request(app).post('/users/register').send({
      username: 'user1',
      email: 'user1@example.com',
      password: 'password',
    });

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });

  it('should return an error if user login fails', async () => {
    const errorMessage = 'No user found!';
    (loginService.login as any).mockRejectedValue(new Error(errorMessage));

    const res = await request(app).post('/users/login').send({
      email: 'user1@example.com',
      password: 'password',
    });

    expect(res.status).toBe(400);
    expect(res.body).toEqual({ error: errorMessage });
  });
});
