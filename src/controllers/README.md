# API Endpoints

## Projects

Get all project's members:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential.

```sh
/GET http://localhost:3000/api/projects/:projectId/members
```

Get all project's sprints:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential.

```sh
/GET http://localhost:3000/api/projects/:projectId/sprints
```

Get all project's tasks:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential.

```sh
/GET http://localhost:3000/api/projects/:projectId/tasks
```

Create project:

> Note: you have to provide token as param and be logged in. You have to provide
> this request body:

```sh
{
    "name": "Project Name"
    "description": "Project Description"
}
```

```sh
/POST http://localhost:3000/api/:token/projects
```

Add project member:

> Note: you have to provide projectId of the project you are member or owner of
> as param and be logged in with member or owner of project credential. You have
> to provide this request body:

```sh
{
    "userId": "Existing User Id"
    "projectId": "Existing Project Id"
}
```

```sh
/POST http://localhost:3000/api/projects/:projectId/members
```

Update project details:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential. You have to provide this request body:

```sh
{
    "name": "Project Name"
    "description": "Project Description"
}
```

```sh
/PUT http://localhost:3000/api/projects/:projectId
```

Delete project:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential.

```sh
/DELETE http://localhost:3000/api/projects/:projectId
```

## Sprints

Get all sprint's tasks:

> Note: you have to provide projectId and sprintId as param and be logged in
> with member or owner of project credential.

```sh
/GET http://localhost:3000/api/:projectId/sprints/:id/tasks
```

Create sprint:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential. You have to provide this request body:

```sh
{
    "name": "Sprint Name",
    "startDate": "Start Date",
    "endDate": "End Date",
    "goal": "Sprint Description",
    "projectId": "Project Id"
}
```

```sh
/POST http://localhost:3000/api/:projectId/sprints
```

Update sprint details:

> Note: you have to provide projectId and sprintId as param and be logged in
> with member or owner of project credential. You have to provide this request
> body:

```sh
{
    "id": "Sprint Id",
    "name": "Sprint 4",
    "startDate": "2024-06-02 16:29:06",
    "endDate": "2024-06-02 16:29:06",
    "goal": "do not do stuff",
    "projectId": 2
}
```

```sh
/PUT http://localhost:3000/api/:projectId/sprints/:id
```

Start sprint:

> Note: you have to provide projectId and sprintId as param and be logged in
> with member or owner of project credential.

```sh
/PUT http://localhost:3000/api/:projectId/sprints/:id/status/start
```

End sprint:

> Note: you have to provide projectId and sprintId as param and be logged in
> with member or owner of project credential. You have to provide this request
> body:

```sh
{
    "nextSprintId": "Next Sprint Id",
}
```

```sh
/PUT http://localhost:3000/api/:projectId/sprints/:id/status/end
```

Delete sprint:

> Note: you have to provide projectId and sprintId as param and be logged in
> with member or owner of project credential.

```sh
/DELETE http://localhost:3000/api/:projectId/sprints/:id
```

## Tasks

Create task:

> Note: you have to provide projectId as param and be logged in with member or
> owner of project credential. You have to provide this request body:

```sh
{
    "title": "Task Title",
    "projectId": "Project Id",
    "sprintId": "Sprint Id"
}
```

```sh
/POST http://localhost:3000/api/:projectId/tasks
```

Update task details:

> Note: you have to provide projectId and taskId as param and be logged in with
> member or owner of project credential. You have to provide this request body:

```sh
{
    "id": "Task Id"
    "title": "Task Title",
}
```

```sh
/PUT http://localhost:3000/api/:projectId/tasks/:id
```

Change task sprint:

> Note: you have to provide projectId and taskId as param and be logged in with
> member or owner of project credential. You have to provide this request body:

```sh
{
    "id": "Task Id",
    "projectId": "Project Id",
    "sprintId": "Sprint Id"
}
```

```sh
/PUT http://localhost:3000/api/:projectId/tasks/:id/sprint
```

Change task assignee:

> Note: you have to provide projectId and taskId as param and be logged in with
> member or owner of project credential. You have to provide this request body:

```sh
{
    "id": "Task Id",
    "projectId": "Project Id",
    "assigneeId": "Assignee Id"
}
```

```sh
/PUT http://localhost:3000/api/:projectId/tasks/:id/assignee
```

Delete task:

> Note: you have to provide projectId and taskId as param and be logged in with
> member or owner of project credential.

```sh
/DELETE http://localhost:3000/api/:projectId/tasks/:id
```

## Users

Get all projects the user is member of:

> Note: you have to provide token as param and be logged in.

```sh
/GET http://localhost:3000/api/users/:token/projects
```

Get user's owned projects:

> Note: you have to provide token as param and be logged in.

```sh
/GET http://localhost:3000/api/users/:token/ownedprojects
```

Get user details:

> Note: you have to provide token as param and be logged in.

```sh
/GET http://localhost:3000/api/users/:token/profile
```

Register user:

> Note: You have to provide this request body:

```sh
{
    "username": "Username",
    "email": "User Email",
    "password": "User Password"
}
```

```sh
/POST http://localhost:3000/api/users/register
```

Update user details:

> Note: you have to provide token as param and be logged in. You have to provide
> this request body:

```sh
{
    "username": "Username",
    "email": "User Email",
    "password": "User Password"
}
```

```sh
/PUT http://localhost:3000/api/users/:token/update
```

Deregister user:

> Note: you have to provide token as param and be logged in.

```sh
/DELETE http://localhost:3000/api/users/:token/delete
```

Login:

> Note: You have to provide this request body:

```sh
{
    "email": "User Email",
    "password": "User Password"
}
```

```sh
/POST http://localhost:3000/api/users/login
```
