import express, { Request, Response } from 'express';
import * as createService from '../services/task/create';
import * as updateService from '../services/task/update';
import * as updateAssignee from '../services/task/updateAssignee';
import * as updateSprint from '../services/task/updateSprint';
import * as deleteService from '../services/task/delete';
import { authorizeMember } from '../middleware/projectMemberAuth';

const router = express.Router();

//Creating a task-------------------------------------------------------------
router.post(
  '/:projectId/tasks',
  authorizeMember,
  async (req: Request, res: Response) => {
    const { title, projectId, sprintId } = req.body;
    const token = req.params.token;

    try {
      const task = await createService.createTask(
        title,
        token,
        projectId,
        sprintId,
      );

      res.json(task);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Updating a task title--------------------------------------------------------
router.put(
  '/:projectId/tasks/:id',
  authorizeMember,
  async (req: Request, res: Response) => {
    const taskId = req.params.id;
    const { title } = req.body;

    try {
      const task = await updateService.updateTask(Number(taskId), title);

      res.json(task);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Changing the sprint for a task------------------------------------------------
router.put(
  '/:projectId/tasks/:id/sprint',
  authorizeMember,
  async (req: Request, res: Response) => {
    const taskId = req.params.id;
    const { projectId, sprintId } = req.body;

    try {
      const task = await updateSprint.updateTaskSprint(
        Number(taskId),
        projectId,
        sprintId,
      );

      res.json(task);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Changing the assignee for a task------------------------------------------------
router.put(
  '/:projectId/tasks/:id/assignee',
  authorizeMember,
  async (req: Request, res: Response) => {
    const taskId = req.params.id;
    const { projectId, assigneeId } = req.body;

    try {
      const task = await updateAssignee.updateTaskAssignee(
        Number(taskId),
        projectId,
        assigneeId,
      );

      res.json(task);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Deleting a task-------------------------------------------------------------------
router.delete(
  '/:projectId/tasks/:id',
  authorizeMember,
  async (req: Request, res: Response) => {
    const taskId = req.params.id;

    try {
      await deleteService.deleteTask(Number(taskId));

      res.status(200).json('OK');
    } catch (error) {
      res.status(500).json({ error: 'Internal server error' });
    }
  },
);

export default router;
