import express, { Request, Response } from 'express';
import * as registerService from '../services/user/register';
import * as updateService from '../services/user/update';
import * as deleteService from '../services/user/delete';
import * as loginService from '../services/user/login';
import * as userProjectsService from '../services/user/userProjects';
import * as ownedProjectsService from '../services/user/ownedProjects';
import * as profileService from '../services/user/profile';
import { authorizeUser } from '../middleware/accountAuth';

const router = express.Router();

//Getting list of projects of a user--------------------------------------------------------
router.get(
  '/users/:token/projects',
  authorizeUser,
  async (req: Request, res: Response) => {
    const token = req.params.token;

    try {
      const projects = await userProjectsService.getUserProjects(token);

      res.json(projects);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Getting list of owned projects of a user---------------------------------------------------
router.get(
  '/users/:token/ownedprojects',
  authorizeUser,
  async (req: Request, res: Response) => {
    const token = req.params.token;

    try {
      const projects = await ownedProjectsService.getOwnedProjects(token);

      res.json(projects);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Get profile------------------------------------------------------------------------------------
router.get(
  '/users/:token/profile',
  authorizeUser,
  async (req: Request, res: Response) => {
    const token = req.params.token;

    try {
      const user = await profileService.profile(token);

      res.json(user);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Registering user---------------------------------------------------------------------------
router.post('/users/register', async (req: Request, res: Response) => {
  const { username, email, password } = req.body;

  try {
    const user = await registerService.registerUser(username, email, password);

    res.json(user);
  } catch (error: any) {
    res.status(400).json({ error: error.message });
  }
});

//Update user---------------------------------------------------------------------------
router.put(
  '/users/:token/update',
  authorizeUser,
  async (req: Request, res: Response) => {
    const { username, email, password } = req.body;
    const token = req.params.token;

    try {
      const user = await updateService.updateUser(
        token,
        username,
        email,
        password,
      );

      res.json(user);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Delete user---------------------------------------------------------------------------
router.delete(
  '/users/:token/delete',
  authorizeUser,
  async (req: Request, res: Response) => {
    const token = req.params.token;

    try {
      const user = await deleteService.deleteUser(token);

      res.json(user);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Login---------------------------------------------------------------------------------------
router.post('/users/login', async (req: Request, res: Response) => {
  const { email, password } = req.body;

  try {
    const token = await loginService.login(email, password);

    res.json(token);
  } catch (error: any) {
    res.status(400).json({ error: error.message });
  }
});

export default router;
