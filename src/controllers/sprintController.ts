import express, { Request, Response } from 'express';
import * as createService from '../services/sprint/create';
import * as updateService from '../services/sprint/update';
import * as updateStatusService from '../services/sprint/updateStatus';
import * as deleteService from '../services/sprint/delete';
import * as sprintTasksService from '../services/sprint/sprintTasks';
import { authorizeMember } from '../middleware/projectMemberAuth';

const router = express.Router();

//Get tasks of a sprint------------------------------------------------------------
router.get(
  '/:projectId/sprints/:id/tasks',
  authorizeMember,
  async (req: Request, res: Response) => {
    const sprintId = req.params.id;

    try {
      const tasks = await sprintTasksService.getSprintTasks(Number(sprintId));

      res.json(tasks);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Creating a sprint-----------------------------------------------------------------
router.post(
  '/:projectId/sprints',
  authorizeMember,
  async (req: Request, res: Response) => {
    const { name, goal, startDate, endDate, projectId } = req.body;

    try {
      const sprint = await createService.createSprint(
        name,
        goal,
        startDate,
        endDate,
        projectId,
      );

      res.json(sprint);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Updating a sprint details----------------------------------------------------------
router.put(
  '/:projectId/sprints/:id',
  authorizeMember,
  async (req: Request, res: Response) => {
    const sprintId = req.params.id;

    const { name, goal, startDate, endDate } = req.body;
    try {
      const sprint = await updateService.updateSprint(
        Number(sprintId),
        name,
        goal,
        startDate,
        endDate,
      );

      res.json(sprint);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Starting a sprint------------------------------------------------------------------
router.put(
  '/:projectId/sprints/:id/status/start',
  authorizeMember,
  async (req: Request, res: Response) => {
    const sprintId = req.params.id;

    try {
      const sprint = await updateStatusService.startSprint(Number(sprintId));

      res.json(sprint);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Ending a sprint---------------------------------------------------------------------
router.put(
  '/:projectId/sprints/:id/status/end',
  authorizeMember,
  async (req: Request, res: Response) => {
    const sprintId = req.params.id;
    const { nextSprintId } = req.body;

    try {
      const sprint = await updateStatusService.endSprint(
        Number(sprintId),
        nextSprintId,
      );

      res.json(sprint);
    } catch (error: any) {
      res.status(400).json({ error: error.message });
    }
  },
);

//Deleting a sprint--------------------------------------------------------------------
router.delete(
  '/:projectId/sprints/:id',
  authorizeMember,
  async (req: Request, res: Response) => {
    const sprintId = req.params.id;

    try {
      await deleteService.deleteSprint(Number(sprintId));

      res.status(200).json('OK');
    } catch (error: any) {
      res.status(500).json({ error: error.message });
    }
  },
);

export default router;
