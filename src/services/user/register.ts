import { User } from '../../models/User';
import bcrypt from 'bcrypt';

export async function registerUser(
  username: string,
  email: string,
  password: string,
): Promise<User> {
  const existingUser = await User.findOne({ where: { email } });

  if (existingUser) {
    throw new Error('Email is already registered');
  }

  const hashedPassword = await bcrypt.hash(password, 10);

  const newUser = await User.create({
    username,
    email,
    password: hashedPassword,
  });

  return newUser;
}
