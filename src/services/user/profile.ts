import { User } from '../../models/User';
import jwt from 'jsonwebtoken';

export async function profile(token: any): Promise<User> {
  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const userId = decoded.userId;

    const user = await User.findByPk(userId);

    if (!user) {
      throw new Error('User not found!');
    }

    return user;
  } catch (error: any) {
    throw new Error(`Failed to get user: ${error.message}`);
  }
}
