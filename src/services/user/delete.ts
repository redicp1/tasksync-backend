import { User } from '../../models/User';
import jwt from 'jsonwebtoken';

export async function deleteUser(token: any): Promise<void> {
  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const userId = decoded.userId;

    const user = await User.findByPk(userId);

    if (!user) {
      throw new Error('User not found!');
    }

    await user.destroy();
  } catch (error: any) {
    throw new Error(`Failed to delete user: ${error.message}`);
  }
}
