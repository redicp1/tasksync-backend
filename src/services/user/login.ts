import { User } from '../../models/User';

import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

export async function login(email: string, password: string) {
  const user = await User.findOne({ where: { email } });

  if (!user) {
    throw new Error('No user found!');
  }

  const passwordMatch = await bcrypt.compare(password, user.password);

  if (!passwordMatch) {
    throw new Error('Password does not match!');
  }

  const token = jwt.sign({ userId: user.id }, String(process.env.SECRET_KEY), {
    expiresIn: '1h',
  });

  return token;
}
