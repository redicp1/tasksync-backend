import { Project } from '../../models/Project';
import { User } from '../../models/User';
import jwt from 'jsonwebtoken';
import redisClient from '../../middleware/cacheClient';

export async function getOwnedProjects(token: any): Promise<Project[]> {
  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const userId = decoded.userId;

    const cacheKey = `user_projects_${userId}`;

    const cachedOwnedProjects = await redisClient.get(cacheKey);

    if (cachedOwnedProjects) {
      return JSON.parse(cachedOwnedProjects);
    }

    const user = await User.findByPk(userId);

    if (!user) {
      throw new Error('User not found');
    }

    const projects = await Project.findAll({
      attributes: ['id', 'name', 'description', 'created'],
      where: {
        ownerId: userId,
      },
    });

    await redisClient.set(cacheKey, JSON.stringify(projects), 'EX', 3600);

    return projects;
  } catch (error) {
    throw new Error(`Failed to get task list: ${(error as Error).message}`);
  }
}
