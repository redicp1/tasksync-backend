import { Project } from '../../models/Project';
import { User } from '../../models/User';
import jwt from 'jsonwebtoken';
import redisClient from '../../middleware/cacheClient';

export async function getUserProjects(token: any): Promise<Project[]> {
  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const userId = decoded.userId;

    const cacheKey = `member_projects_${userId}`;

    const cachedMemberProjects = await redisClient.get(cacheKey);

    if (cachedMemberProjects) {
      return JSON.parse(cachedMemberProjects);
    }

    const user = await User.findByPk(userId, {
      include: [
        {
          model: Project,
          as: 'projects',
          attributes: ['id', 'name', 'description', 'created'],
        },
      ],
    });

    if (!user) {
      throw new Error('User not found');
    }

    await redisClient.set(cacheKey, JSON.stringify(user.projects), 'EX', 3600);

    return user.projects;
  } catch (error) {
    throw new Error(`Failed to get task list: ${(error as Error).message}`);
  }
}
