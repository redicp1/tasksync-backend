import { User } from '../../models/User';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

export async function updateUser(
  token: any,
  username: string,
  email: string,
  password: string,
): Promise<User> {
  const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
  const userId = decoded.userId;

  const user = await User.findByPk(userId);

  if (!user) {
    throw new Error('User not found');
  }

  const existingUser = await User.findOne({ where: { email } });

  if (existingUser) {
    throw new Error('Email is already registered');
  }

  const hashedPassword = await bcrypt.hash(password, 10);

  user.username = username;
  user.email = email;
  user.password = hashedPassword;

  user.save();

  return user;
}
