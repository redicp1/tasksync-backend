import { registerUser } from '../register';
import { User } from '../../../models/User';
import bcrypt from 'bcrypt';
import { vi, describe, it, expect } from 'vitest';

// Mock the models and dependencies
vi.mock('../../../models/User', () => ({
  User: {
    findOne: vi.fn(),
    create: vi.fn(),
  },
}));
vi.mock('bcrypt', () => ({
  hash: vi.fn(),
}));

describe('register user', () => {
  //   it('should register a new user if email is not registered', async () => {
  //     const username = 'testuser';
  //     const email = 'test@example.com';
  //     const password = 'password';
  //     const hashedPassword = 'hashedPassword';
  //     const newUser = { id: 1, username, email, password: hashedPassword };

  //     (User.findOne as any).mockResolvedValue(null);
  //     (bcrypt.hash as any).mockResolvedValue(hashedPassword);
  //     (User.create as any).mockResolvedValue(newUser);

  //     const result = await registerUser(username, email, password);

  //     expect(User.findOne).toHaveBeenCalledWith({ where: { email } });
  //     expect(bcrypt.hash).toHaveBeenCalledWith(password, 10);
  //     expect(User.create).toHaveBeenCalledWith({
  //       username,
  //       email,
  //       password: hashedPassword,
  //     });
  //     expect(result).toEqual(newUser);
  //   });

  it('should throw an error if email is already registered', async () => {
    const username = 'testuser';
    const email = 'test@example.com';
    const password = 'password';
    const existingUser = { id: 1, username, email, password: 'hashedPassword' };
    const errorMessage = 'Email is already registered';

    (User.findOne as any).mockResolvedValue(existingUser);

    await expect(registerUser(username, email, password)).rejects.toThrow(
      errorMessage,
    );
  });

  it('should throw a generic error if any other error occurs', async () => {
    const username = 'testuser';
    const email = 'test@example.com';
    const password = 'password';
    const errorMessage = 'Database error';

    (User.findOne as any).mockRejectedValue(new Error(errorMessage));

    await expect(registerUser(username, email, password)).rejects.toThrow(
      errorMessage,
    );
  });
});
