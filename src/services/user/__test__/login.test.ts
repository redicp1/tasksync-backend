import { login } from '../login';
import { User } from '../../../models/User';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { vi, describe, it, expect } from 'vitest';

// Mock the models and dependencies
vi.mock('../../../models/User', () => ({
  User: {
    findOne: vi.fn(),
  },
}));
vi.mock('bcrypt', () => ({
  compare: vi.fn(),
  hash: vi.fn(),
}));
vi.mock('jsonwebtoken', () => ({
  sign: vi.fn(),
}));

describe('login', () => {
  //   it('should login user and return a token', async () => {
  //     const email = 'test@example.com';
  //     const password = 'password';
  //     const user = { id: 1, email, password: await bcrypt.hash(password, 10) };
  //     const token = 'test-token';

  //     (User.findOne as any).mockResolvedValue(user);
  //     (bcrypt.compare as any).mockResolvedValue(true);
  //     (jwt.sign as any).mockReturnValue(token);

  //     const result = await login(email, password);

  //     expect(User.findOne).toHaveBeenCalledWith({ where: { email } });
  //     expect(bcrypt.compare).toHaveBeenCalledWith(password, user.password);
  //     expect(jwt.sign).toHaveBeenCalledWith(
  //       { userId: user.id },
  //       String(process.env.SECRET_KEY),
  //       {
  //         expiresIn: '1h',
  //       },
  //     );
  //     expect(result).toEqual(token);
  //   });

  it('should throw an error if no user found', async () => {
    const email = 'test@example.com';
    const password = 'password';
    const errorMessage = 'No user found!';

    (User.findOne as any).mockResolvedValue(null);

    await expect(login(email, password)).rejects.toThrow(errorMessage);
  });

  //   it('should throw an error if password does not match', async () => {
  //     const email = 'test@example.com';
  //     const password = 'password';
  //     const user = {
  //       id: 1,
  //       email,
  //       password: await bcrypt.hash('wrongpassword', 10),
  //     };
  //     const errorMessage = 'Password does not match!';

  //     (User.findOne as any).mockResolvedValue(user);
  //     (bcrypt.compare as any).mockResolvedValue(false);

  //     await expect(login(email, password)).rejects.toThrow(errorMessage);
  //   });

  it('should throw a generic error if any other error occurs', async () => {
    const email = 'test@example.com';
    const password = 'password';
    const errorMessage = 'Database error';

    (User.findOne as any).mockRejectedValue(new Error(errorMessage));

    await expect(login(email, password)).rejects.toThrow(errorMessage);
  });
});
