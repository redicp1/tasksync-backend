import redisClient from '../../middleware/cacheClient';
import { Task } from '../../models/Task';
import { UserProject } from '../../models/UserProject';

export async function updateTaskAssignee(
  taskId: number,
  projectId: number,
  assigneeId?: number,
): Promise<Task> {
  try {
    const task = await Task.findOne({
      where: { id: taskId, projectId: projectId },
    });

    if (!task) {
      throw new Error('Task not found!');
    }

    const sprintId = task?.sprintId;

    if (assigneeId) {
      const userExists = await UserProject.findOne({
        where: {
          userId: assigneeId,
          projectId: projectId,
        },
      });

      if (!userExists) {
        throw new Error('User not found!');
      }
    }

    task.assigneeId = assigneeId ?? null;

    task.save();

    await redisClient.del(`sprint_tasks_${sprintId}`);
    await redisClient.del(`project_tasks_${projectId}`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to update task: ${error.message}`);
  }
}
