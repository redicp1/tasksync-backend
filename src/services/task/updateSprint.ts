import redisClient from '../../middleware/cacheClient';
import { Sprint } from '../../models/Sprint';
import { Task } from '../../models/Task';

export async function updateTaskSprint(
  taskId: number,
  projectId: number,
  sprintId?: number,
): Promise<Task> {
  try {
    const task = await Task.findOne({
      where: { id: taskId, projectId: projectId },
    });

    if (!task) {
      throw new Error('Task not found!');
    }

    if (sprintId) {
      const sprint = await Sprint.findOne({
        where: { id: sprintId, projectId: projectId },
      });

      if (!sprint) {
        throw new Error('Sprint not found!');
      }
    }

    task.sprintId = sprintId ?? null;

    task.save();

    await redisClient.del(`sprint_tasks_${sprintId}`);
    await redisClient.del(`project_tasks_${projectId}`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to update task: ${error.message}`);
  }
}
