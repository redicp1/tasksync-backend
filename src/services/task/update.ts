import redisClient from '../../middleware/cacheClient';
import { Task } from '../../models/Task';

export async function updateTask(taskId: number, title: string): Promise<Task> {
  try {
    const task = await Task.findByPk(taskId);

    if (!task) {
      throw new Error('Task not found!');
    }

    const sprintId = task?.sprintId;
    const projectId = task.projectId;

    task.title = title;

    task.save();

    await redisClient.del(`sprint_tasks_${sprintId}`);
    await redisClient.del(`project_tasks_${projectId}`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to update task: ${error.message}`);
  }
}
