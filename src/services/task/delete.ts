import redisClient from '../../middleware/cacheClient';
import { Task } from '../../models/Task';

export async function deleteTask(taskId: number): Promise<void> {
  try {
    const task = await Task.findByPk(taskId);

    if (!task) {
      throw new Error('Task not found!');
    }

    const sprintId = task?.sprintId;
    const projectId = task.projectId;

    await task.destroy();

    await redisClient.del(`sprint_tasks_${sprintId}`);
    await redisClient.del(`project_tasks_${projectId}`);
  } catch (error: any) {
    throw new Error(`Failed to delete task: ${error.message}`);
  }
}
