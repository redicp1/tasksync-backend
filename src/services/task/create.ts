import redisClient from '../../middleware/cacheClient';
import { Project } from '../../models/Project';
import { Sprint } from '../../models/Sprint';
import { Task } from '../../models/Task';
import { User } from '../../models/User';
import jwt from 'jsonwebtoken';

export async function createTask(
  title: string,
  token: any,
  projectId: number,
  sprintId?: number,
): Promise<Task> {
  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const authorId = decoded.userId;

    const project = await Project.findByPk(projectId);

    if (!project) {
      throw new Error('Project not found!');
    }

    const author = await User.findByPk(authorId);

    if (!author) {
      throw new Error('User not found!');
    }

    if (sprintId) {
      const sprint = await Sprint.findOne({
        where: { id: sprintId, projectId: projectId },
      });

      if (!sprint) {
        throw new Error('Sprint not found!');
      }
    }

    const task = await Task.create({ title, authorId, sprintId, projectId });

    await redisClient.del(`sprint_tasks_${sprintId}`);
    await redisClient.del(`project_tasks_${projectId}`);

    return task;
  } catch (error: any) {
    throw new Error(`Failed to create task: ${error.message}`);
  }
}
