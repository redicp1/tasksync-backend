import { deleteTask } from '../delete';
import { Task } from '../../../models/Task';
import { vi, describe, it, expect } from 'vitest';
import { beforeEach } from 'node:test';

// Mock the models
vi.mock('../../../models/Task', () => ({
  Task: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('delete task', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should delete a task if it exists', async () => {
    const mockDestroy = vi.fn();
    const taskId = 1;
    const task = { id: taskId };

    (Task.findByPk as any).mockResolvedValue({ destroy: mockDestroy });

    await deleteTask(taskId);

    expect(Task.findByPk).toHaveBeenCalledWith(taskId);
    expect(mockDestroy).toHaveBeenCalled();
  });

  it('should throw an error if the task is not found', async () => {
    const taskId = 1;
    const errorMessage = 'Task not found!';
    (Task.findByPk as any).mockResolvedValue(null);

    await expect(deleteTask(taskId)).rejects.toThrow(errorMessage);
  });

  it('should throw a generic error if any other error occurs', async () => {
    const taskId = 1;
    const errorMessage = 'Database error';
    (Task.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(deleteTask(taskId)).rejects.toThrow(
      `Failed to delete task: ${errorMessage}`,
    );
  });
});
