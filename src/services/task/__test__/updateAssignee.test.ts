import { updateTaskAssignee } from '../updateAssignee';
import { Task } from '../../../models/Task';
import { UserProject } from '../../../models/UserProject';
import { vi, describe, it, expect } from 'vitest';
import { beforeEach } from 'node:test';

// Mock the models
vi.mock('../../../models/Task', () => ({
  Task: {
    findOne: vi.fn(),
  },
}));

vi.mock('../../../models/UserProject', () => ({
  UserProject: {
    findOne: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('update assignee of a task', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should update task assignee if task and user exists', async () => {
    const mockSave = vi.fn();

    const taskId = 1;
    const projectId = 1;
    const assigneeId = 1;

    const task = { id: taskId, assigneeId: null, save: mockSave };
    const userProject = { userId: assigneeId, projectId };

    (Task.findOne as any).mockResolvedValue(task);
    (UserProject.findOne as any).mockResolvedValue(userProject);

    const updatedTask = await updateTaskAssignee(taskId, projectId, assigneeId);

    expect(Task.findOne).toHaveBeenCalledWith({
      where: { id: taskId, projectId: projectId },
    });
    expect(UserProject.findOne).toHaveBeenCalledWith({
      where: { userId: assigneeId, projectId: projectId },
    });
    expect(task.assigneeId).toEqual(assigneeId);
    expect(mockSave).toHaveBeenCalled();
    expect(updatedTask).toEqual(task);
  });

  it('should remove task assignee if assigneeId is not provided', async () => {
    const mockSave = vi.fn();

    const taskId = 1;
    const projectId = 1;

    const task = { id: taskId, assigneeId: 1, save: mockSave };

    (Task.findOne as any).mockResolvedValue(task);

    const updatedTask = await updateTaskAssignee(taskId, projectId);

    expect(Task.findOne).toHaveBeenCalledWith({
      where: { id: taskId, projectId: projectId },
    });
    expect(task.assigneeId).toBeNull();
    expect(mockSave).toHaveBeenCalled();
    expect(updatedTask).toEqual(task);
  });

  it('should throw an error if task is not found', async () => {
    const taskId = 1;
    const projectId = 1;
    const errorMessage = 'Task not found!';

    (Task.findOne as any).mockResolvedValue(null);

    await expect(updateTaskAssignee(taskId, projectId, 1)).rejects.toThrow(
      errorMessage,
    );
  });

  it('should throw an error if user is not found', async () => {
    const taskId = 1;
    const projectId = 1;
    const assigneeId = 1;
    const errorMessage = 'User not found!';

    (Task.findOne as any).mockResolvedValue({ id: taskId, assigneeId: null });
    (UserProject.findOne as any).mockResolvedValue(null);

    await expect(
      updateTaskAssignee(taskId, projectId, assigneeId),
    ).rejects.toThrow(errorMessage);
  });

  it('should throw a generic error if any other error occurs', async () => {
    const taskId = 1;
    const projectId = 1;
    const errorMessage = 'Database error';

    (Task.findOne as any).mockRejectedValue(new Error(errorMessage));

    await expect(updateTaskAssignee(taskId, projectId, 1)).rejects.toThrow(
      `Failed to update task: ${errorMessage}`,
    );
  });
});
