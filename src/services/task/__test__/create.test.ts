import { vi, describe, it, expect } from 'vitest';
import { createTask } from '../create';
import { Project } from '../../../models/Project';
import { Sprint } from '../../../models/Sprint';
import { Task } from '../../../models/Task';
import { User } from '../../../models/User';
import { beforeEach } from 'node:test';

// Mock the models
vi.mock('../../../models/Project', () => ({
  Project: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../models/Sprint', () => ({
  Sprint: {
    findOne: vi.fn(),
  },
}));

vi.mock('../../../models/Task', () => ({
  Task: {
    create: vi.fn(),
  },
}));

vi.mock('../../../models/User', () => ({
  User: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('create task', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  const projectId = 1;
  const sprintId = 1;
  const authorId = 1;
  const title = 'Task 1';

  it('should create a task if project and user are found', async () => {
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    (User.findByPk as any).mockResolvedValue({ id: authorId });
    (Sprint.findOne as any).mockResolvedValue({ id: sprintId, projectId });

    const task = { id: 1, title, authorId, projectId, sprintId };
    (Task.create as any).mockResolvedValue(task);

    const result = await createTask(title, authorId, projectId, sprintId);

    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(User.findByPk).toHaveBeenCalledWith(authorId);
    expect(Sprint.findOne).toHaveBeenCalledWith({
      where: { id: sprintId, projectId: projectId },
    });
    expect(Task.create).toHaveBeenCalledWith({
      title,
      authorId,
      projectId,
      sprintId,
    });
    expect(result).toEqual(task);
  });

  it('should throw an error if project is not found', async () => {
    const errorMessage = 'Project not found!';
    (Project.findByPk as any).mockResolvedValue(null);

    await expect(
      createTask(title, authorId, projectId, sprintId),
    ).rejects.toThrow(errorMessage);
  });

  it('should throw an error if user is not found', async () => {
    const errorMessage = 'User not found!';
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    (User.findByPk as any).mockResolvedValue(null);

    await expect(
      createTask(title, authorId, projectId, sprintId),
    ).rejects.toThrow(errorMessage);
  });

  it('should throw an error if sprint is not found', async () => {
    const errorMessage = 'Sprint not found!';
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    (User.findByPk as any).mockResolvedValue({ id: authorId });
    (Sprint.findOne as any).mockResolvedValue(null);

    await expect(
      createTask(title, authorId, projectId, sprintId),
    ).rejects.toThrow(errorMessage);
  });

  it('should create a task without sprint if sprintId is not provided', async () => {
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    (User.findByPk as any).mockResolvedValue({ id: authorId });
    (Sprint.findOne as any).mockResolvedValue(null);

    const task = { id: 1, title, authorId, projectId };
    (Task.create as any).mockResolvedValue(task);

    const result = await createTask(title, authorId, projectId);

    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(User.findByPk).toHaveBeenCalledWith(authorId);
    expect(Task.create).toHaveBeenCalledWith({ title, authorId, projectId });
    expect(result).toEqual(task);
  });

  it('should throw a generic error if any other error occurs', async () => {
    const errorMessage = 'Database error';
    (Project.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(
      createTask(title, authorId, projectId, sprintId),
    ).rejects.toThrow(`Failed to create task: ${errorMessage}`);
  });
});
