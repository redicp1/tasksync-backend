import { updateTask } from '../update';
import { Task } from '../../../models/Task';
import { vi, describe, it, expect } from 'vitest';
import { beforeEach } from 'node:test';

// Mock the models
vi.mock('../../../models/Task', () => ({
  Task: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('update task', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should update a task if it exists', async () => {
    const mockSave = vi.fn();

    const taskId = 1;
    const title = 'Updated Task';
    const task = { id: taskId, title: 'Old Task', save: mockSave };

    (Task.findByPk as any).mockResolvedValue(task);

    const updatedTask = await updateTask(taskId, title);

    expect(Task.findByPk).toHaveBeenCalledWith(taskId);
    expect(task.title).toEqual(title);
    expect(mockSave).toHaveBeenCalled();
    expect(updatedTask).toEqual(task);
  });

  it('should throw an error if the task is not found', async () => {
    const taskId = 1;
    const errorMessage = 'Task not found!';
    (Task.findByPk as any).mockResolvedValue(null);

    await expect(updateTask(taskId, 'Updated Task')).rejects.toThrow(
      errorMessage,
    );
  });

  it('should throw a generic error if any other error occurs', async () => {
    const taskId = 1;
    const errorMessage = 'Database error';
    (Task.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(updateTask(taskId, 'Updated Task')).rejects.toThrow(
      `Failed to update task: ${errorMessage}`,
    );
  });
});
