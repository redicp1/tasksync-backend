import { updateTaskSprint } from '../updateSprint';
import { Sprint } from '../../../models/Sprint';
import { Task } from '../../../models/Task';
import { vi, describe, it, expect, beforeEach } from 'vitest';

// Mock the models
vi.mock('../../../models/Sprint', () => ({
  Sprint: {
    findOne: vi.fn(),
  },
}));

vi.mock('../../../models/Task', () => ({
  Task: {
    findOne: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('update sprint of a task', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should update task sprint if task and sprint exist', async () => {
    const mockSave = vi.fn();

    const taskId = 1;
    const projectId = 1;
    const sprintId = 1;

    const task = { id: taskId, sprintId: null, save: mockSave };
    const sprint = { id: sprintId, projectId };

    (Task.findOne as any).mockResolvedValue(task);
    (Sprint.findOne as any).mockResolvedValue(sprint);

    const updatedTask = await updateTaskSprint(taskId, projectId, sprintId);

    expect(Task.findOne).toHaveBeenCalledWith({
      where: { id: taskId, projectId: projectId },
    });
    expect(Sprint.findOne).toHaveBeenCalledWith({
      where: { id: sprintId, projectId: projectId },
    });
    expect(task.sprintId).toEqual(sprintId);
    expect(mockSave).toHaveBeenCalled();
    expect(updatedTask).toEqual(task);
  });

  it('should remove task sprint if sprintId is not provided', async () => {
    const mockSave = vi.fn();

    const taskId = 1;
    const projectId = 1;

    const task = { id: taskId, sprintId: 1, save: mockSave };

    (Task.findOne as any).mockResolvedValue(task);

    const updatedTask = await updateTaskSprint(taskId, projectId);

    expect(Task.findOne).toHaveBeenCalledWith({
      where: { id: taskId, projectId: projectId },
    });
    expect(task.sprintId).toBeNull();
    expect(mockSave).toHaveBeenCalled();
    expect(updatedTask).toEqual(task);
  });

  it('should throw an error if task is not found', async () => {
    const taskId = 1;
    const projectId = 1;
    const errorMessage = 'Task not found!';

    (Task.findOne as any).mockResolvedValue(null);

    await expect(updateTaskSprint(taskId, projectId, 1)).rejects.toThrow(
      errorMessage,
    );
  });

  it('should throw an error if sprint is not found', async () => {
    const taskId = 1;
    const projectId = 1;
    const sprintId = 1;
    const errorMessage = 'Sprint not found!';

    (Task.findOne as any).mockResolvedValue({ id: taskId, sprintId: null });
    (Sprint.findOne as any).mockResolvedValue(null);

    await expect(updateTaskSprint(taskId, projectId, sprintId)).rejects.toThrow(
      errorMessage,
    );
  });

  it('should throw a generic error if any other error occurs', async () => {
    const taskId = 1;
    const projectId = 1;
    const errorMessage = 'Database error';

    (Task.findOne as any).mockRejectedValue(new Error(errorMessage));

    await expect(updateTaskSprint(taskId, projectId, 1)).rejects.toThrow(
      `Failed to update task: ${errorMessage}`,
    );
  });
});
