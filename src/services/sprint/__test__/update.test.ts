import { updateSprint } from '../update';
import { Sprint } from '../../../models/Sprint';
import { vi, describe, it, expect } from 'vitest';
import { beforeEach } from 'node:test';

// Mock the models
vi.mock('../../../models/Sprint', () => ({
  Sprint: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('update sprint', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should update a sprint if it exists', async () => {
    const mockSave = vi.fn();

    const sprintId = 1;
    const name = 'Updated Sprint';
    const goal = 'Updated goal';
    const startDate = new Date('2023-06-01');
    const endDate = new Date('2023-06-15');
    const sprint = {
      id: sprintId,
      name: 'Old Sprint',
      goal: 'Old goal',
      startDate: new Date(),
      endDate: new Date(),
      save: mockSave,
    };

    (Sprint.findByPk as any).mockResolvedValue(sprint);

    const updatedSprint = await updateSprint(
      sprintId,
      name,
      goal,
      startDate,
      endDate,
    );

    expect(Sprint.findByPk).toHaveBeenCalledWith(sprintId);
    expect(updatedSprint.name).toEqual(name);
    expect(updatedSprint.goal).toEqual(goal);
    expect(updatedSprint.startDate).toEqual(startDate);
    expect(updatedSprint.endDate).toEqual(endDate);
    expect(mockSave).toHaveBeenCalled();
  });

  it('should throw an error if the sprint is not found', async () => {
    const sprintId = 1;
    const errorMessage = 'Sprint not found!';
    (Sprint.findByPk as any).mockResolvedValue(null);

    await expect(
      updateSprint(
        sprintId,
        'Updated Sprint',
        'Updated goal',
        new Date('2023-06-01'),
        new Date('2023-06-15'),
      ),
    ).rejects.toThrow(errorMessage);
  });

  it('should throw a generic error if any other error occurs', async () => {
    const sprintId = 1;
    const errorMessage = 'Database error';
    (Sprint.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(
      updateSprint(
        sprintId,
        'Updated Sprint',
        'Updated goal',
        new Date('2023-06-01'),
        new Date('2023-06-15'),
      ),
    ).rejects.toThrow(`Failed to update sprint: ${errorMessage}`);
  });
});
