import { createSprint } from '../create';
import { Sprint } from '../../../models/Sprint';
import { Project } from '../../../models/Project';
import { vi, describe, it, expect } from 'vitest';
import { beforeEach } from 'node:test';

// Mock the models
vi.mock('../../../models/Sprint', () => ({
  Sprint: {
    create: vi.fn(),
  },
}));
vi.mock('../../../models/Project', () => ({
  Project: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('create sprint', () => {
  const projectId = 1;
  const name = 'Sprint 1';
  const goal = 'Complete user authentication';
  const startDate = new Date('2023-06-01');
  const endDate = new Date('2023-06-15');

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should throw an error if the project is not found', async () => {
    const errorMessage = 'Project not found!';
    (Project.findByPk as any).mockResolvedValue(null);

    await expect(
      createSprint(name, goal, startDate, endDate, projectId),
    ).rejects.toThrow(errorMessage);
  });

  it('should create a sprint if the project is found', async () => {
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    const sprint = { id: 1, name, goal, startDate, endDate, projectId };
    (Sprint.create as any).mockResolvedValue(sprint);

    const result = await createSprint(
      name,
      goal,
      startDate,
      endDate,
      projectId,
    );

    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(Sprint.create).toHaveBeenCalledWith({
      name,
      goal,
      startDate,
      endDate,
      projectId,
    });
    expect(result).toEqual(sprint);
  });

  it('should throw a generic error if any other error occurs', async () => {
    const errorMessage = 'Database error';
    (Project.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(
      createSprint(name, goal, startDate, endDate, projectId),
    ).rejects.toThrow(`Failed to create sprint: ${errorMessage}`);
  });
});
