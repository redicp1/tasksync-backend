import { getSprintTasks } from '../sprintTasks';
import { Task } from '../../../models/Task';
import { Sprint } from '../../../models/Sprint';
import { beforeEach, vi, describe, it, expect } from 'vitest';
import redisClient from '../../../middleware/cacheClient';

// Mock the models
vi.mock('../../../models/Task', () => ({
  Task: {
    findAll: vi.fn(),
  },
}));
vi.mock('../../../models/Sprint', () => ({
  Sprint: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('get tasks of a sprint', () => {
  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should return members from cache if available', async () => {
    const sprintId = 1;
    const cacheKey = `sprint_tasks_${sprintId}`;
    const tasks = [
      { id: 1, title: 'Task 1' },
      { id: 2, title: 'Task 2' },
    ];

    (redisClient.get as any).mockResolvedValueOnce(JSON.stringify(tasks));

    const result = await getSprintTasks(sprintId);

    expect(result).toEqual(tasks);
    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Sprint.findByPk).not.toHaveBeenCalled();
  });

  it('should return tasks for a given sprint', async () => {
    const sprintId = 1;
    const tasks = [
      { id: 1, title: 'Task 1' },
      { id: 2, title: 'Task 2' },
    ];
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Sprint.findByPk as any).mockResolvedValue({ id: sprintId });
    (Task.findAll as any).mockResolvedValue(tasks);

    const result = await getSprintTasks(sprintId);

    expect(Sprint.findByPk).toHaveBeenCalledWith(sprintId);
    expect(Task.findAll).toHaveBeenCalledWith({
      attributes: ['id', 'title'],
      where: {
        sprintId: sprintId,
      },
    });
    expect(result).toEqual(tasks);
  });

  it('should throw an error if the sprint is not found', async () => {
    const sprintId = 1;
    const errorMessage = 'Sprint not found';

    (redisClient.get as any).mockResolvedValueOnce(null);
    (Sprint.findByPk as any).mockResolvedValue(null);

    await expect(getSprintTasks(sprintId)).rejects.toThrow(errorMessage);
  });

  it('should throw a generic error if any other error occurs', async () => {
    const sprintId = 1;
    const errorMessage = 'Database error';

    (redisClient.get as any).mockResolvedValueOnce(null);
    (Sprint.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(getSprintTasks(sprintId)).rejects.toThrow(
      `Failed to get task list: ${errorMessage}`,
    );
  });
});
