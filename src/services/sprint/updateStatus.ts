import { Transaction } from 'sequelize';
import sequelize from '../../database/connection';
import { Sprint } from '../../models/Sprint';
import { SprintStatus } from '../../models/SprintStatus';
import { Task } from '../../models/Task';

async function updateStatus(
  sprintId: number,
  status: SprintStatus,
  transaction?: Transaction,
): Promise<Sprint> {
  try {
    const sprint = await Sprint.findByPk(sprintId);

    if (!sprint) {
      throw new Error('Sprint not found!');
    }

    sprint.status = status;

    sprint.save({ transaction });

    return sprint;
  } catch (error: any) {
    throw new Error(`Failed to update sprint's status: ${error.message}`);
  }
}

export async function startSprint(sprintId: number) {
  updateStatus(sprintId, SprintStatus.RUNNING);
}

export async function endSprint(
  sprintId: number,
  nextSprintId?: number,
): Promise<void> {
  if (sprintId === nextSprintId) {
    throw new Error(`Choose sprint other than the current sprint`);
  }

  const sprint = await Sprint.findByPk(sprintId);
  if (!sprint) {
    throw new Error(`Sprint with id ${sprintId} not found`);
  }

  const transaction: Transaction = await sequelize.transaction();

  const tasks = await Task.findAll({ where: { sprintId: sprintId } });

  try {
    if (!tasks) {
      updateStatus(sprintId, SprintStatus.COMPLETE);

      await transaction.commit();
    } else {
      await sequelize.query(
        'UPDATE Task SET sprintId = :nextSprintId WHERE sprintId = :currentSprintId',
        {
          replacements: {
            nextSprintId: nextSprintId ?? null,
            currentSprintId: sprintId,
          },
          transaction,
        },
      );

      updateStatus(sprintId, SprintStatus.COMPLETE);

      await transaction.commit();
    }
  } catch (error: any) {
    await transaction.rollback();
    throw new Error(
      `Failed to delete sprint with id ${sprintId}: ${error.message}`,
    );
  }
}
