import { Task } from '../../models/Task';
import { Sprint } from '../../models/Sprint';
import redisClient from '../../middleware/cacheClient';

export async function getSprintTasks(sprintId: number): Promise<Task[]> {
  try {
    const cacheKey = `sprint_tasks_${sprintId}`;

    const cachedSprintTasks = await redisClient.get(cacheKey);

    if (cachedSprintTasks) {
      return JSON.parse(cachedSprintTasks);
    }

    const sprint = await Sprint.findByPk(sprintId);

    if (!sprint) {
      throw new Error('Sprint not found');
    }

    const tasks = await Task.findAll({
      attributes: ['id', 'title'],
      where: {
        sprintId: sprintId,
      },
    });

    await redisClient.set(cacheKey, JSON.stringify(tasks), 'EX', 3600);

    return tasks;
  } catch (error) {
    throw new Error(`Failed to get task list: ${(error as Error).message}`);
  }
}
