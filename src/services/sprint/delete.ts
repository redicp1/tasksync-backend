import { Transaction } from 'sequelize';
import sequelize from '../../database/connection';
import { Sprint } from '../../models/Sprint';
import redisClient from '../../middleware/cacheClient';

export async function deleteSprint(sprintId: number): Promise<void> {
  const sprint = await Sprint.findByPk(sprintId);
  if (!sprint) {
    throw new Error(`Sprint with id ${sprintId} not found`);
  }

  const projectId = sprint.projectId;

  const transaction: Transaction = await sequelize.transaction();

  try {
    if (!sprint.tasks || sprint.tasks.length == 0) {
      await sprint.destroy({ transaction });

      await transaction.commit();
    } else {
      await sequelize.query(
        'UPDATE Task SET sprintId = NULL WHERE sprintId = :sprintId',
        {
          replacements: { sprintId },
          transaction,
        },
      );

      await sprint.destroy({ transaction });

      await transaction.commit();

      await redisClient.del(`project_sprints_${projectId}`);
    }
  } catch (error: any) {
    await transaction.rollback();
    throw new Error(
      `Failed to delete sprint with id ${sprintId}: ${error.message}`,
    );
  }
}
