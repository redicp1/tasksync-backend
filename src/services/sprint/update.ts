import redisClient from '../../middleware/cacheClient';
import { Sprint } from '../../models/Sprint';

export async function updateSprint(
  sprintId: number,
  name: string,
  goal: string,
  startDate: Date,
  endDate: Date,
): Promise<Sprint> {
  try {
    const sprint = await Sprint.findByPk(sprintId);

    if (!sprint) {
      throw new Error('Sprint not found!');
    }

    const projectId = sprint.projectId;

    sprint.name = name;
    sprint.goal = goal;
    sprint.startDate = startDate;
    sprint.endDate = endDate;

    sprint.save();

    await redisClient.del(`project_sprints_${projectId}`);

    return sprint;
  } catch (error: any) {
    throw new Error(`Failed to update sprint: ${error.message}`);
  }
}
