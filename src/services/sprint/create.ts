import { Sprint } from '../../models/Sprint';
import { Project } from '../../models/Project';
import redisClient from '../../middleware/cacheClient';

export async function createSprint(
  name: string,
  goal: string,
  startDate: Date,
  endDate: Date,
  projectId: number,
): Promise<Sprint> {
  try {
    const project = await Project.findByPk(projectId);

    if (!project) {
      throw new Error('Project not found!');
    }

    const sprint = await Sprint.create({
      name,
      startDate,
      endDate,
      goal,
      projectId,
    });

    await redisClient.del(`project_sprints_${projectId}`);

    return sprint;
  } catch (error: any) {
    throw new Error(`Failed to create sprint: ${error.message}`);
  }
}
