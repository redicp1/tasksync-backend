import redisClient from '../../middleware/cacheClient';
import { Project } from '../../models/Project';

export async function updateProject(
  projectId: number,
  name: string,
  description: string,
): Promise<Project> {
  try {
    const project = await Project.findByPk(projectId);

    if (!project) {
      throw new Error('Project not found!');
    }

    const ownerId = project.ownerId;

    project.name = name;
    project.description = description;

    project.save();

    await redisClient.del(`user_projects_${ownerId}`);

    return project;
  } catch (error: any) {
    throw new Error(`Failed to update project: ${error.message}`);
  }
}
