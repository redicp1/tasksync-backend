import { User } from '../../models/User';
import { Project } from '../../models/Project';
import redisClient from '../../middleware/cacheClient';

export async function getProjectMembers(projectId: number): Promise<User[]> {
  try {
    const cacheKey = `project_members_${projectId}`;

    const cachedMembers = await redisClient.get(cacheKey);

    if (cachedMembers) {
      return JSON.parse(cachedMembers);
    }

    const project = await Project.findByPk(projectId, {
      include: [
        {
          model: User,
          as: 'members',
          attributes: ['id', 'username'],
          through: { attributes: [] },
        },
      ],
    });

    if (!project) {
      throw new Error('Project not found');
    }

    await redisClient.set(
      cacheKey,
      JSON.stringify(project.members),
      'EX',
      3600,
    );

    return project.members;
  } catch (error) {
    throw new Error(`Failed to get member list: ${(error as Error).message}`);
  }
}
