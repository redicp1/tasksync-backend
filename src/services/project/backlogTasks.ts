import { Task } from '../../models/Task';
import { Project } from '../../models/Project';
import redisClient from '../../middleware/cacheClient';

export async function getBacklogTasks(projectId: number): Promise<Task[]> {
  try {
    const cacheKey = `project_tasks_${projectId}`;

    const cachedProjectTasks = await redisClient.get(cacheKey);

    if (cachedProjectTasks) {
      return JSON.parse(cachedProjectTasks);
    }

    const project = await Project.findByPk(projectId);

    if (!project) {
      throw new Error('Project not found');
    }

    const tasks = await Task.findAll({
      attributes: ['id', 'title'],
      where: {
        projectId: projectId,
        sprintId: null,
      },
    });

    await redisClient.set(cacheKey, JSON.stringify(tasks), 'EX', 3600);

    return tasks;
  } catch (error) {
    throw new Error(`Failed to get task list: ${(error as Error).message}`);
  }
}
