import redisClient from '../../middleware/cacheClient';
import { Project } from '../../models/Project';
import { User } from '../../models/User';
import { UserProject } from '../../models/UserProject';

export async function addProjectMember(
  userId: number,
  projectId: number,
): Promise<void> {
  try {
    const user = await User.findByPk(userId);

    if (!user) {
      throw new Error('User not found!');
    }

    const project = await Project.findByPk(projectId);

    if (!project) {
      throw new Error('Project not found!');
    }

    const projectMember = await UserProject.findOne({
      where: {
        userId: userId,
        projectId: projectId,
      },
    });

    if (projectMember) {
      throw new Error('Member already exists in the project');
    }

    await UserProject.create({ userId, projectId });

    await redisClient.del(`project_members_${projectId}`);
  } catch (error: any) {
    throw new Error(`Failed to add member to project: ${error.message}`);
  }
}
