import redisClient from '../../middleware/cacheClient';
import { Project } from '../../models/Project';

export async function deleteProject(projectId: number): Promise<void> {
  try {
    const project = await Project.findByPk(projectId);

    const ownerId = project?.ownerId;

    if (!project) {
      throw new Error('Project not found!');
    }

    await project.destroy();

    await redisClient.del(`user_projects_${ownerId}`);
  } catch (error: any) {
    throw new Error(`Failed to delete project: ${error.message}`);
  }
}
