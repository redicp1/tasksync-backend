import redisClient from '../../middleware/cacheClient';
import { Project } from '../../models/Project';
import { User } from '../../models/User';
import jwt from 'jsonwebtoken';

export async function createProject(
  name: string,
  description: string,
  token: any,
): Promise<Project> {
  try {
    const decoded: any = jwt.verify(token, String(process.env.SECRET_KEY));
    const ownerId = decoded.userId;

    const user = await User.findByPk(ownerId);

    if (!user) {
      throw new Error('User not found!');
    }

    const project = await Project.create({ name, description, ownerId });

    await redisClient.del(`user_projects_${ownerId}`);

    return project;
  } catch (error: any) {
    throw new Error(`Failed to create project: ${error.message}`);
  }
}
