import { describe, it, expect, vi, beforeEach } from 'vitest';
import { getBacklogTasks } from '../backlogTasks'; // adjust the path accordingly
import { Task } from '../../../models/Task';
import { Project } from '../../../models/Project';
import redisClient from '../../../middleware/cacheClient';

vi.mock('../../../models/Task');
vi.mock('../../../models/Project');
vi.mock('../../../middleware/cacheClient');

describe('getBacklogTasks', () => {
  const projectId = 1;
  const cacheKey = `project_tasks_${projectId}`;
  const tasks = [
    { id: 1, title: 'Task 1' },
    { id: 2, title: 'Task 2' },
  ];

  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('should return tasks from cache if available', async () => {
    (redisClient.get as any).mockResolvedValueOnce(JSON.stringify(tasks));

    const result = await getBacklogTasks(projectId);

    expect(result).toEqual(tasks);
    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).not.toHaveBeenCalled();
    expect(Task.findAll).not.toHaveBeenCalled();
  });

  it('should fetch tasks from database if not in cache', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValueOnce({ id: projectId });
    (Task.findAll as any).mockResolvedValueOnce(tasks);
    (redisClient.set as any).mockResolvedValueOnce(null);

    const result = await getBacklogTasks(projectId);

    expect(result).toEqual(tasks);
    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(Task.findAll).toHaveBeenCalledWith({
      attributes: ['id', 'title'],
      where: {
        projectId: projectId,
        sprintId: null,
      },
    });
    expect(redisClient.set).toHaveBeenCalledWith(
      cacheKey,
      JSON.stringify(tasks),
      'EX',
      3600,
    );
  });

  it('should throw an error if project is not found', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValueOnce(null);

    await expect(getBacklogTasks(projectId)).rejects.toThrow(
      'Project not found',
    );

    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(Task.findAll).not.toHaveBeenCalled();
    expect(redisClient.set).not.toHaveBeenCalled();
  });

  it('should throw an error if there is a failure in fetching tasks', async () => {
    const errorMessage = 'Database error';
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValueOnce({ id: projectId });
    (Task.findAll as any).mockRejectedValueOnce(new Error(errorMessage));

    await expect(getBacklogTasks(projectId)).rejects.toThrow(
      `Failed to get task list: ${errorMessage}`,
    );

    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(Task.findAll).toHaveBeenCalledWith({
      attributes: ['id', 'title'],
      where: {
        projectId: projectId,
        sprintId: null,
      },
    });
    expect(redisClient.set).not.toHaveBeenCalled();
  });
});
