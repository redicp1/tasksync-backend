import { describe, it, expect, vi, beforeEach } from 'vitest';
import { createProject } from '../create';
import { Project } from '../../../models/Project';
import { User } from '../../../models/User';
import redisClient from '../../../middleware/cacheClient';

// Mock the models
vi.mock('../../../models/User', () => ({
  User: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../models/Project', () => ({
  Project: {
    create: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('create project', () => {
  const ownerId = 1;
  const name = 'Project 1';
  const description = 'Description 1';

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should throw an error if the user is not found', async () => {
    (User.findByPk as any).mockResolvedValue(null);

    await expect(createProject(name, description, ownerId)).rejects.toThrow(
      'User not found',
    );
  });

  it('should create project if user is found', async () => {
    (User.findByPk as any).mockResolvedValue({ id: ownerId });
    (Project.create as any).mockResolvedValue({ name, description, ownerId });

    await createProject(name, description, ownerId);

    expect(Project.create).toHaveBeenCalledWith({ name, description, ownerId });
  });

  it('should throw a generic error if any other error occurs', async () => {
    const errorMessage = 'Database error';
    (User.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(createProject(name, description, ownerId)).rejects.toThrow(
      `Failed to create project: ${errorMessage}`,
    );
  });
});
