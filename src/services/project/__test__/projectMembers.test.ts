import { describe, it, expect, vi, beforeEach } from 'vitest';
import { getProjectMembers } from '../projectMembers'; // adjust the path accordingly
import { User } from '../../../models/User';
import { Project } from '../../../models/Project';
import redisClient from '../../../middleware/cacheClient';

vi.mock('../../../models/User');
vi.mock('../../../models/Project');
vi.mock('../../../middleware/cacheClient');

describe('getProjectMembers', () => {
  const projectId = 1;
  const cacheKey = `project_members_${projectId}`;
  const members = [
    { id: 1, username: 'user1' },
    { id: 2, username: 'user2' },
  ];

  beforeEach(() => {
    vi.clearAllMocks();
  });

  it('should return members from cache if available', async () => {
    (redisClient.get as any).mockResolvedValueOnce(JSON.stringify(members));

    const result = await getProjectMembers(projectId);

    expect(result).toEqual(members);
    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).not.toHaveBeenCalled();
  });

  it('should fetch members from database if not in cache', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValueOnce({
      id: projectId,
      members,
    } as any);
    (redisClient.set as any).mockResolvedValueOnce(null);

    const result = await getProjectMembers(projectId);

    expect(result).toEqual(members);
    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).toHaveBeenCalledWith(projectId, {
      include: [
        {
          model: User,
          as: 'members',
          attributes: ['id', 'username'],
          through: { attributes: [] },
        },
      ],
    });
    expect(redisClient.set).toHaveBeenCalledWith(
      cacheKey,
      JSON.stringify(members),
      'EX',
      3600,
    );
  });

  it('should throw an error if project is not found', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValueOnce(null);

    await expect(getProjectMembers(projectId)).rejects.toThrow(
      'Project not found',
    );

    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).toHaveBeenCalledWith(projectId, {
      include: [
        {
          model: User,
          as: 'members',
          attributes: ['id', 'username'],
          through: { attributes: [] },
        },
      ],
    });
    expect(redisClient.set).not.toHaveBeenCalled();
  });

  it('should throw an error if there is a failure in fetching members', async () => {
    const errorMessage = 'Database error';
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockRejectedValueOnce(new Error(errorMessage));

    await expect(getProjectMembers(projectId)).rejects.toThrow(
      `Failed to get member list: ${errorMessage}`,
    );

    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).toHaveBeenCalledWith(projectId, {
      include: [
        {
          model: User,
          as: 'members',
          attributes: ['id', 'username'],
          through: { attributes: [] },
        },
      ],
    });
    expect(redisClient.set).not.toHaveBeenCalled();
  });
});
