import { describe, it, expect, vi, beforeEach } from 'vitest';
import { addProjectMember } from '../addProjectMember';
import { Project } from '../../../models/Project';
import { User } from '../../../models/User';
import { UserProject } from '../../../models/UserProject';

// Mock the models
vi.mock('../../../models/Project', () => ({
  Project: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../models/User', () => ({
  User: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../models/UserProject', () => ({
  UserProject: {
    findOne: vi.fn(),
    create: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('add a member to a project', () => {
  const userId = 1;
  const projectId = 1;

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should throw an error if the user is not found', async () => {
    (User.findByPk as any).mockResolvedValue(null);

    await expect(addProjectMember(userId, projectId)).rejects.toThrow(
      'User not found!',
    );
  });

  it('should throw an error if the project is not found', async () => {
    (User.findByPk as any).mockResolvedValue({ id: userId });
    (Project.findByPk as any).mockResolvedValue(null);

    await expect(addProjectMember(userId, projectId)).rejects.toThrow(
      'Project not found!',
    );
  });

  it('should throw an error if the member already exists in the project', async () => {
    (User.findByPk as any).mockResolvedValue({ id: userId });
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    (UserProject.findOne as any).mockResolvedValue({ userId, projectId });

    await expect(addProjectMember(userId, projectId)).rejects.toThrow(
      'Member already exists in the project',
    );
  });

  it('should add the user to the project if they are not already a member', async () => {
    (User.findByPk as any).mockResolvedValue({ id: userId });
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    (UserProject.findOne as any).mockResolvedValue(null);
    (UserProject.create as any).mockResolvedValue({ userId, projectId });

    await addProjectMember(userId, projectId);

    expect(UserProject.create).toHaveBeenCalledWith({ userId, projectId });
  });

  it('should throw a generic error if any other error occurs', async () => {
    const errorMessage = 'Database error';
    (User.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(addProjectMember(userId, projectId)).rejects.toThrow(
      `Failed to add member to project: ${errorMessage}`,
    );
  });
});
