import { describe, it, expect, vi, beforeEach } from 'vitest';
import { deleteProject } from '../delete';
import { Project } from '../../../models/Project';

// Mock the models
vi.mock('../../../models/Project', () => ({
  Project: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('delete project', () => {
  const projectId = 1;

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should throw an error if the project is not found', async () => {
    (Project.findByPk as any).mockResolvedValue(null);

    await expect(deleteProject(projectId)).rejects.toThrow(
      'Project not found!',
    );
  });

  it('should delete delete project if the project is found', async () => {
    const mockDestroy = vi.fn();
    (Project.findByPk as any).mockResolvedValue({ destroy: mockDestroy });

    await deleteProject(projectId);

    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(mockDestroy).toHaveBeenCalled();
  });

  it('should throw a generic error if any other error occurs', async () => {
    const errorMessage = 'Database error';
    (Project.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(deleteProject(projectId)).rejects.toThrow(
      `Failed to delete project: ${errorMessage}`,
    );
  });
});
