import { describe, it, expect, vi, beforeEach } from 'vitest';
import { updateProject } from '../update';
import { Project } from '../../../models/Project';

// Mock the models
vi.mock('../../../models/Project', () => ({
  Project: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('updateProject', () => {
  const projectId = 1;
  const name = 'Updated Project Name';
  const description = 'Updated Project Description';

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should throw an error if the project is not found', async () => {
    (Project.findByPk as any).mockResolvedValue(null);

    await expect(updateProject(projectId, name, description)).rejects.toThrow(
      'Project not found!',
    );
  });

  it('should update the project if it is found', async () => {
    const mockSave = vi.fn();
    const project = {
      id: projectId,
      name: 'Old Name',
      description: 'Old Description',
      save: mockSave,
    };
    (Project.findByPk as any).mockResolvedValue(project);

    const updatedProject = await updateProject(projectId, name, description);

    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(updatedProject.name).toBe(name);
    expect(updatedProject.description).toBe(description);
    expect(mockSave).toHaveBeenCalled();
  });

  it('should throw a generic error if any other error occurs', async () => {
    const errorMessage = 'Database error';
    (Project.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(updateProject(projectId, name, description)).rejects.toThrow(
      `Failed to update project: ${errorMessage}`,
    );
  });
});
