import { describe, it, expect, vi, beforeEach } from 'vitest';
import { getProjectSprints } from '../projectSprints';
import { Sprint } from '../../../models/Sprint';
import { Project } from '../../../models/Project';
import redisClient from '../../../middleware/cacheClient';
import jwt from 'jsonwebtoken';

// Mock the models
vi.mock('../../../models/Project', () => ({
  Project: {
    findByPk: vi.fn(),
  },
}));

vi.mock('../../../models/Sprint', () => ({
  Sprint: {
    findAll: vi.fn(),
  },
}));

vi.mock('../../../middleware/cacheClient');

describe('getProjectSprints', () => {
  const projectId = 1;
  const cacheKey = `project_sprints_${projectId}`;

  beforeEach(() => {
    vi.resetAllMocks();
  });

  it('should return members from cache if available', async () => {
    const sprints = [
      { id: 1, name: 'Sprint 1', status: 'active' },
      { id: 2, name: 'Sprint 2', status: 'completed' },
    ];

    (redisClient.get as any).mockResolvedValueOnce(JSON.stringify(sprints));

    const result = await getProjectSprints(projectId);

    expect(result).toEqual(sprints);
    expect(redisClient.get).toHaveBeenCalledWith(cacheKey);
    expect(Project.findByPk).not.toHaveBeenCalled();
  });

  it('should throw an error if the project is not found', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValue(null);

    await expect(getProjectSprints(projectId)).rejects.toThrow(
      'Project not found',
    );
  });

  it('should return the sprints if the project is found', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    (Project.findByPk as any).mockResolvedValue({ id: projectId });
    const sprints = [
      { id: 1, name: 'Sprint 1', status: 'active' },
      { id: 2, name: 'Sprint 2', status: 'completed' },
    ];
    (Sprint.findAll as any).mockResolvedValue(sprints);

    const result = await getProjectSprints(projectId);

    expect(Project.findByPk).toHaveBeenCalledWith(projectId);
    expect(Sprint.findAll).toHaveBeenCalledWith({
      attributes: ['id', 'name', 'status'],
      where: { projectId },
    });
    expect(result).toEqual(sprints);
  });

  it('should throw a generic error if any other error occurs', async () => {
    (redisClient.get as any).mockResolvedValueOnce(null);
    const errorMessage = 'Database error';
    (Project.findByPk as any).mockRejectedValue(new Error(errorMessage));

    await expect(getProjectSprints(projectId)).rejects.toThrow(
      `Failed to get sprint list: ${errorMessage}`,
    );
  });
});
