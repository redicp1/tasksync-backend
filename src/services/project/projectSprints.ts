import { Sprint } from '../../models/Sprint';
import { Project } from '../../models/Project';
import redisClient from '../../middleware/cacheClient';

export async function getProjectSprints(projectId: number): Promise<Sprint[]> {
  try {
    const cacheKey = `project_sprints_${projectId}`;

    const cachedSprints = await redisClient.get(cacheKey);

    if (cachedSprints) {
      return JSON.parse(cachedSprints);
    }

    const project = await Project.findByPk(projectId);

    if (!project) {
      throw new Error('Project not found');
    }

    const sprints = await Sprint.findAll({
      attributes: ['id', 'name', 'status'],
      where: {
        projectId: projectId,
      },
    });

    await redisClient.set(cacheKey, JSON.stringify(sprints), 'EX', 3600);

    return sprints;
  } catch (error) {
    throw new Error(`Failed to get sprint list: ${(error as Error).message}`);
  }
}
