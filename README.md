# TaskSync Backend

The repository contains backend part of the TaskSync: Collaborative Task
Manager.

## Software Requirement

In order to run the TaskSync project in development mode on Linux, macOS or
Windows, the following must be installed.

- [Docker][]
- [Docker Compose][]
- [NodeJS 18][nodejs]

Optional:

- [Azure CLI][azurecli]
- [Kubectl][]

## Getting started

Clone and setup the project.

> Note: your CLI should have elevated privileges when setting up and starting
> the app

```sh
git clone https://git.fhict.nl/I464936/tasksync-backend.git
npm install
```

The project requires a MySQL database and Redis server. This project contains a
Docker Compose configuration to spin up a preconfigured database with ease. You
can comment out the app part.

```sh
docker compose up -d
```

The project can be served using the following command.

```sh
npm run dev
```

## Test and Deploy

The project can be tested using the following command.

```sh
npm test
```

For getting coverage for the implemetation.

```sh
npm test:coverage
```

The project can be deployed to dockerhub.

```sh
docker login
docker build -t tasksync .
docker tag tasksync redicr/tasksync:latest
docker push redicr/tasksync:latest
```

# Kubernetes & Azure Deployment

The folder of K8s contains deployment files and service files configurations
that were used in the Azure Cloud Kubernetes Cluster.

pre-requirements:

1. Download kubectl for cluster communication.

Access the [Kubernetes Cluster][kubernetes]

To communicate wiht the cluster, the following steps need to be taken:

1. Download [Azure CLI][azurecli]
2. Login to with azure CLI: in the command line use:

```sh
az login
```

3. Login with the corresponding account into Azure environment and access the
   cluster. Execute the following commands in the "Connect" section to connect
   to the cluster via the console.
4. Test the connection: (kubectl required), following command "kubectl get pods"
   should give an overview of running pods in default namespace in the Azure
   Cluster.

# CI/CD

The structure of the pipeline can be found in the "gitlab-ci.yaml" and contains
the following steps:

1. build stage
2. test stage
3. sonarcloud code analysis
4. load testing with k6 cloud
5. deployment of the images to DockerHub Registry
6. deployment of kubernetes cluster in azure portal

[docker]: https://docker.com
[docker compose]: https://docs.docker.com/compose
[nodejs]: https://nodejs.org
[azurecli]:
  https://learn.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest
[kubernetes]:
  https://portal.azure.com/#@fhictsky.onmicrosoft.com/resource/subscriptions/c91261a6-6dbd-428d-a3e0-583a3edd9ba6/resourceGroups/TaskSync/providers/Microsoft.ContainerService/managedClusters/TaskSyncCluster/overview
[kubectl]: https://kubernetes.io/docs/tasks/tools/
