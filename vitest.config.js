import { defineConfig } from 'vitest/config';

export default defineConfig({
  test: {
    globals: true,
    environment: 'node',
    reporters: ['default', 'junit'], // Configures default and JUnit reporters
    outputFile: 'junit.xml', // Specifies the output file for the JUnit reporter
    coverage: {
      provider: 'v8', // Uses V8 as the coverage provider
      reporter: ['lcov'], // Configures multiple coverage report formats
      exclude: ['build/**', 'node_modules/**'],
    },
    exclude: ['build/**', 'node_modules/**'],
  },
});
